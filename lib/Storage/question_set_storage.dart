import 'package:mock_test_flutter/configs/imports/package_imports.dart';

class IndexStorage {
  final String _key = 'currentIndex';
  final _storage = GetStorage();

  int getIndex() {
    return _storage.read(_key) ?? 3;
  }

  void saveIndex(int index) {
    _storage.write(_key, index);
  }
}
