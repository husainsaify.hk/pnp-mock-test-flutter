// To parse this JSON data, do
//
//     final resultModel = resultModelFromJson(jsonString);

import 'dart:convert';

ResultModel resultModelFromJson(String str) =>
    ResultModel.fromJson(json.decode(str));

String resultModelToJson(ResultModel data) => json.encode(data.toJson());

class ResultModel {
  int status;
  String message;
  Data data;

  ResultModel({
    required this.status,
    required this.message,
    required this.data,
  });

  factory ResultModel.fromJson(Map<String, dynamic> json) => ResultModel(
        status: json["status"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": data.toJson(),
      };
}

class Data {
  String id;
  String userId;
  int totalMarks;
  double marksObtained;
  int attemptedQuestionsCount;
  int notAttemptedQuestionsCount;
  int wrongAnswersCount;
  int correctAnswersCount;
  int totalPositiveMarks;
  double totalNegativeMarks;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  List<QuestionAnswer> questionAnswers;

  Data({
    required this.id,
    required this.userId,
    required this.totalMarks,
    required this.marksObtained,
    required this.attemptedQuestionsCount,
    required this.notAttemptedQuestionsCount,
    required this.wrongAnswersCount,
    required this.correctAnswersCount,
    required this.totalPositiveMarks,
    required this.totalNegativeMarks,
    required this.createdAt,
    required this.updatedAt,
    required this.v,
    required this.questionAnswers,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["_id"],
        userId: json["user_id"],
        totalMarks: json["total_marks"],
        marksObtained: json["marks_obtained"]?.toDouble(),
        attemptedQuestionsCount: json["attempted_questions_count"],
        notAttemptedQuestionsCount: json["not_attempted_questions_count"],
        wrongAnswersCount: json["wrong_answers_count"],
        correctAnswersCount: json["correct_answers_count"],
        totalPositiveMarks: json["total_positive_marks"],
        totalNegativeMarks: json["total_negative_marks"]?.toDouble(),
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
        questionAnswers: List<QuestionAnswer>.from(
            json["question_answers"].map((x) => QuestionAnswer.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "user_id": userId,
        "total_marks": totalMarks,
        "marks_obtained": marksObtained,
        "attempted_questions_count": attemptedQuestionsCount,
        "not_attempted_questions_count": notAttemptedQuestionsCount,
        "wrong_answers_count": wrongAnswersCount,
        "correct_answers_count": correctAnswersCount,
        "total_positive_marks": totalPositiveMarks,
        "total_negative_marks": totalNegativeMarks,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
        "question_answer": List<Map<String, dynamic>>.from(
            questionAnswers.map((x) => x.toJson())),
      };
}

class QuestionAnswer {
  String id;
  String resultId;
  String questionId;
  String correctAnswer;
  String answerGiven;
  double marksObtained;
  int v;
  DateTime createdAt;
  DateTime updatedAt;

  QuestionAnswer({
    required this.id,
    required this.resultId,
    required this.questionId,
    required this.correctAnswer,
    required this.answerGiven,
    required this.marksObtained,
    required this.v,
    required this.createdAt,
    required this.updatedAt,
  });

  factory QuestionAnswer.fromJson(Map<String, dynamic> json) => QuestionAnswer(
        id: json["_id"],
        resultId: json["result_id"],
        questionId: json["question_id"],
        correctAnswer: json["correct_answer"],
        answerGiven: json["answer_given"],
        marksObtained: json["marks_obtained"]?.toDouble(),
        v: json["__v"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "result_id": resultId,
        "question_id": questionId,
        "correct_answer": correctAnswer,
        "answer_given": answerGiven,
        "marks_obtained": marksObtained,
        "__v": v,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
      };
}

class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap = map.map((k, v) => MapEntry(v, k));
    return reverseMap;
  }
}
