import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  final int status;
  final String message;
  final Data data;

  UserModel({
    required this.status,
    required this.message,
    required this.data,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        status: json["status"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": data.toJson(),
      };
}

class Data {
  final String id;
  final String name;
  final String email;
  final List<String> ongoingTests;
  final List<String> results;
  final List<dynamic> purchasedSet;
  final int points;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int v;

  Data({
    required this.id,
    required this.name,
    required this.email,
    required this.ongoingTests,
    required this.results,
    required this.purchasedSet,
    required this.points,
    required this.createdAt,
    required this.updatedAt,
    required this.v,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["_id"],
        name: json["name"],
        email: json["email"],
        ongoingTests: List<String>.from(json["ongoing_tests"].map((x) => x)),
        results: List<String>.from(json["results"].map((x) => x)),
        purchasedSet: List<dynamic>.from(json["purchased_set"].map((x) => x)),
        points: json["points"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "name": name,
        "email": email,
        "ongoing_tests": List<String>.from(ongoingTests.map((x) => x)),
        "results": List<String>.from(results.map((x) => x)),
        "purchased_set": List<dynamic>.from(purchasedSet.map((x) => x)),
        "points": points,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
      };
}
