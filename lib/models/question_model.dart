import 'dart:convert';

QuestionSet questionSetFromJson(String str) =>
    QuestionSet.fromJson(json.decode(str));

String questionSetToJson(QuestionSet data) => json.encode(data.toJson());

class QuestionSet {
  final QuestionDetailsPaginated questionDetailsPaginated;
  final Myvalue myvalue;
  final List<String> error;
  final List<String> msg;

  QuestionSet({
    required this.questionDetailsPaginated,
    required this.myvalue,
    required this.error,
    required this.msg,
  });

  factory QuestionSet.fromJson(Map<String, dynamic> json) => QuestionSet(
        questionDetailsPaginated:
            QuestionDetailsPaginated.fromJson(json["questionDetailsPaginated"]),
        myvalue: Myvalue.fromJson(json["myvalue"]),
        error: List<String>.from(json["error"].map((x) => x)),
        msg: List<String>.from(json["msg"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "questionDetailsPaginated": questionDetailsPaginated.toJson(),
        "myvalue": myvalue.toJson(),
        "error": List<String>.from(error.map((x) => x)),
        "msg": List<String>.from(msg.map((x) => x)),
      };
}

class Myvalue {
  Myvalue();

  factory Myvalue.fromJson(Map<String, dynamic> json) => Myvalue();

  Map<String, dynamic> toJson() => {};
}

class QuestionDetailsPaginated {
  final List<Doc> docs;
  final int totalDocs;
  final int limit;
  final int totalPages;
  final int page;
  final int pagingCounter;
  final bool hasPrevPage;
  final bool hasNextPage;
  final dynamic prevPage;
  final int nextPage;

  QuestionDetailsPaginated({
    required this.docs,
    required this.totalDocs,
    required this.limit,
    required this.totalPages,
    required this.page,
    required this.pagingCounter,
    required this.hasPrevPage,
    required this.hasNextPage,
    required this.prevPage,
    required this.nextPage,
  });

  factory QuestionDetailsPaginated.fromJson(Map<String, dynamic> json) =>
      QuestionDetailsPaginated(
        docs: List<Doc>.from(json["docs"].map((x) => Doc.fromJson(x))),
        totalDocs: json["totalDocs"],
        limit: json["limit"],
        totalPages: json["totalPages"],
        page: json["page"],
        pagingCounter: json["pagingCounter"],
        hasPrevPage: json["hasPrevPage"],
        hasNextPage: json["hasNextPage"],
        prevPage: json["prevPage"],
        nextPage: json["nextPage"],
      );

  Map<String, dynamic> toJson() => {
        "docs": List<Map<String, dynamic>>.from(docs.map((x) => x.toJson())),
        "totalDocs": totalDocs,
        "limit": limit,
        "totalPages": totalPages,
        "page": page,
        "pagingCounter": pagingCounter,
        "hasPrevPage": hasPrevPage,
        "hasNextPage": hasNextPage,
        "prevPage": prevPage,
        "nextPage": nextPage,
      };
}

class Doc {
  final String id;
  final int sNo;
  final DocText text;
  final List<Option> options;
  final String correctAnswerId;
  final int marksPerQuestion;
  final double minusMarksPerQuestion;
  final String explanationInEnglish;
  final bool active;
  final int v;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String docId;
  // final String id;
  // final TextM text;
  // final List<Option> options;
  // final String correctAnswerId;
  // final int marksPerQuestion;
  // final double minusMarksPerQuestion;
  // final String explanationInEnglish;
  // final bool active;
  // final int v;
  // final DateTime createdAt;
  // final DateTime updatedAt;
  // final String docId;

  Doc({
    required this.id,
    required this.sNo,
    required this.text,
    required this.options,
    required this.correctAnswerId,
    required this.marksPerQuestion,
    required this.minusMarksPerQuestion,
    required this.explanationInEnglish,
    required this.active,
    required this.v,
    required this.createdAt,
    required this.updatedAt,
    required this.docId,
  });

  factory Doc.fromJson(Map<String, dynamic> json) => Doc(
        id: json["_id"],
        sNo: json["s_no"],
        text: DocText.fromJson(json["text"]),
        options:
            List<Option>.from(json["options"].map((x) => Option.fromJson(x))),
        correctAnswerId: json["correct_answer_id"],
        marksPerQuestion: json["marks_per_question"],
        minusMarksPerQuestion: json["minus_marks_per_question"]?.toDouble(),
        explanationInEnglish: json["explanation_in_english"],
        active: json["active"],
        v: json["__v"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        docId: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "s_no": sNo,
        "text": text.toJson(),
        "options":
            List<Map<String, dynamic>>.from(options.map((x) => x.toJson())),
        "correct_answer_id": correctAnswerId,
        "marks_per_question": marksPerQuestion,
        "minus_marks_per_question": minusMarksPerQuestion,
        "explanation_in_english": explanationInEnglish,
        "active": active,
        "__v": v,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "id": docId,
      };
}

class Option {
  final String id;
  final OptionText text;

  Option({
    required this.id,
    required this.text,
  });

  factory Option.fromJson(Map<String, dynamic> json) => Option(
        id: json["id"],
        text: OptionText.fromJson(json["text"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "text": text.toJson(),
      };
}

class OptionText {
  final String en;
  final String? img;
  final String? imgurl;

  OptionText({
    required this.en,
    required this.img,
    required this.imgurl,
  });

  factory OptionText.fromJson(Map<String, dynamic> json) => OptionText(
        en: json["EN"]!,
        img: json["img"],
        imgurl: json["imgurl"],
      );

  Map<String, dynamic> toJson() => {
        "EN": en,
        "img": img,
        "imgurl": imgurl,
      };
}

class DocText {
  final String en;

  DocText({
    required this.en,
  });

  factory DocText.fromJson(Map<String, dynamic> json) => DocText(
        en: json["EN"]!,
      );

  Map<String, dynamic> toJson() => {
        "EN": en,
      };
}

class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap = map.map((k, v) => MapEntry(v, k));
    return reverseMap;
  }
}
