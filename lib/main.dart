import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

void main() async {
  await GetStorage.init();
  InitialBinding().dependencies();
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations(
    [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown],
  ).then((_) {
    runApp(const MyApp());
  });
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  static final GlobalKey<NavigatorState> navigatorKey = GlobalKey();

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final ThemeController _themeController = Get.find();
 
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Obx(
        () => GetMaterialApp(
          navigatorKey: MyApp.navigatorKey,
          theme: _themeController.lightTheme.value,
          darkTheme: _themeController.darkTheme.value,
          getPages: Routes.getPages(),
          debugShowCheckedModeBanner: false,
          initialRoute: Routes.splashScreen,
        ),
      ),
    );
  }
}
