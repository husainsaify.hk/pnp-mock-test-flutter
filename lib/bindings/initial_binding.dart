import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';

class InitialBinding implements Bindings {
  @override
  void dependencies() async {
    Get.put(ThemeController());
    Get.put(ConnectionManagerController());
    Get.put(ResultSummaryController());
    Get.put(QuizResultController());
    Get.put(AuthController(), permanent: true);
    Get.put(IndexController());
    Get.put(QuestionSetController());
    Get.put(OptionController());
    Get.put(AppLauncherController());
    Get.put(AppInitializeController());
  }
}
