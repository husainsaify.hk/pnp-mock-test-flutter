import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/view_imports.dart';
import 'package:mock_test_flutter/view/Profile/contact_us.dart';

class Routes {
  static String homePage = "/homePage";
  static String contactUs = "/contactUs";
  static String splashScreen = "/splashScreen";
  static String quizScreen = "/quizScreen";
  static String overAllView = "/overAllView";
  static String resultScreen = "/resultScreen";
  static String quizExplanation = "/quizExplanation";
  static String signUp = "/signUp";
  static String noInternet = "/noInternet";
  static String login = "/login";
  static String myApp = "/myApp";

  static List<GetPage> getPages() => [
        GetPage(
          name: Routes.myApp,
          page: () => const MyApp(),
        ),
        GetPage(
          name: Routes.homePage,
          page: () => const HomeScreen(),
        ),
        GetPage(
          name: Routes.splashScreen,
          page: () => const SplashScreen(),
        ),
        GetPage(
          name: Routes.signUp,
          page: () => const SignUp(),
        ),
        GetPage(
          name: Routes.login,
          page: () => const LoginScreen(),
        ),
        GetPage(
          name: Routes.overAllView,
          page: () => const OverAllView(),
        ),
        GetPage(
          name: Routes.resultScreen,
          page: () => const ResultScreen(),
        ),
        GetPage(
          name: Routes.quizExplanation,
          page: () => const QuizExplanation(),
        ),
        GetPage(
          name: Routes.quizScreen,
          page: () => const QuizScreen(),
        ),
        GetPage(
          name: Routes.noInternet,
          page: () => const InternetScreen(),
        ),
        GetPage(
          name: Routes.contactUs,
          page: () => const ContactUs(),
        ),
      ];
}
