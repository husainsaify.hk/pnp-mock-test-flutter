// ignore_for_file: constant_identifier_names

const baseUrl = "http://pmp-mock-test.hackerkernel.co/api";
const imageBaseUrl = "http://pmp-mock-test.hackerkernel.co";
const timeout = Duration(seconds: 10);
const email = "mockquestion.in@gmail.com";
const contactNo = "6207040145";
const whatsappContactNo = "6207040145";
const appPrivacyPolicy =
    "https://www.freeprivacypolicy.com/live/4f1f0311-f7ef-4e8c-b955-2b486f5a890e";
const appTermAndConditions =
    "https://www.freeprivacypolicy.com/live/4f1f0311-f7ef-4e8c-b955-2b486f5a890e";
const linkTNC = "https://www.mockquestion.in/terms/";
const linksCR = "https://www.mockquestion.in/return-refund-policy/";
const linksSND = "https://www.mockquestion.in/return-refund-policy/";
const appName = "MOCK QUESTION";
