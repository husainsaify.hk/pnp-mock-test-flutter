import 'package:flutter/material.dart';
import 'package:get/get.dart';

const kDesktopChangePoint = 1100.0;
const kTabletChangePoint = 500.0;
const kMobileChangePoint = 420.0;
const kSmallMobileChangePoint = 250.0;
const kMobileScreenPadding = 25.0;
const kButtonCornerRadius = 10.0;
const kCardBorderrRadius = 10.0;

class UIParameters {
  static EdgeInsets get screenPadding =>
      const EdgeInsets.all(kMobileScreenPadding);

  static double getHeight(BuildContext context) {
    return Get.size.height;
  }

  static double getWidth(BuildContext context) {
    return Get.size.width;
  }

  static bool isDesktop(BuildContext context) {
    final screenWidth = Get.size.width;
    return (kDesktopChangePoint <= screenWidth);
  }

  static bool isMobile(BuildContext context) {
    final screenWidth = Get.size.width;
    return (kTabletChangePoint > screenWidth);
  }

  static bool isTablet(BuildContext context) {
    final screenWidth = Get.size.width;
    return (kTabletChangePoint <= screenWidth);
  }

  static List<BoxShadow> getShadow(
          {Color? shadowColor,
          double spreadRadius = 3,
          double blurRadius = 12}) =>
      [
        BoxShadow(
          color: shadowColor ?? Get.theme.shadowColor,
          spreadRadius: spreadRadius,
          blurRadius: blurRadius,
          offset: const Offset(0, 3),
        ),
      ];
}

class RD {
  final double d;
  final double t;
  final double m;

  const RD({required this.d, required this.t, required this.m});

  double get(BuildContext context) {
    final screenWidth = Get.size.width;
    if (kDesktopChangePoint <= screenWidth) {
      return d;
    } else if (kTabletChangePoint <= screenWidth) {
      return t;
    } else {
      return m;
    }
  }
}

class RWP {
  final double d;
  final double t;
  final double m;

  const RWP({required this.d, required this.t, required this.m});

  double get(BuildContext context) {
    final screenWidth = Get.size.width;
    if (kDesktopChangePoint <= screenWidth) {
      return d * screenWidth;
    } else if (kTabletChangePoint <= screenWidth) {
      return t * screenWidth;
    } else {
      return m * screenWidth;
    }
  }
}
