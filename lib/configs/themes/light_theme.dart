import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';

const MaterialColor kPrimayColorLT = MaterialColor(0xFF0CA7FF, {
  50: Color(0xFFF3FBFF),
  100: Color(0xFFE7F7FF),
  200: Color(0xFFC3E9FF),
  300: Color(0xFF9CDBFF),
  400: Color(0xFF55C2FF),
  500: Color(0xFF0CA7FF),
  600: Color(0xFF0B95E3),
  700: Color(0xFF086599),
  800: Color(0xFF064C73),
  900: Color(0xFF04314A),
});

Color kPrimayLightColorLT = kPrimayColorLT.shade500;
Color appBarIconColorLT = kPrimayColorLT.shade200;
Color backgroundColorLT = kPrimayColorLT.shade50;
Color mainTextColorLT = const Color.fromARGB(255, 0, 29, 44);
Color iconColorLT = kPrimayColorLT.shade900;
Color shadowColorLT = kPrimayColorLT.shade500;
Color scaffoldBackgroundColorLT = kPrimayColorLT.shade50;
Color borderColorLT = kPrimayColorLT.shade100;

//? Container
Color primaryContainerLT = const Color.fromARGB(255, 255, 255, 255);

class LightTheme {
  static ThemeData buildLightTheme() {
    return ThemeData(
      brightness: Brightness.light,
      scaffoldBackgroundColor: scaffoldBackgroundColorLT,
      shadowColor: shadowColorLT.withOpacity(0.2),

      visualDensity: VisualDensity.adaptivePlatformDensity,
      splashColor: kPrimayColorLT.withOpacity(0.1),
      splashFactory: InkRipple.splashFactory,
      highlightColor: kPrimayColorLT.withOpacity(0.05),

      //? Icon Theme Data

      iconTheme: IconThemeData(
        color: iconColorLT,
      ),

      //? Icon Button Theme Data

      iconButtonTheme: IconButtonThemeData(
        style: ButtonStyle(
          iconColor: MaterialStatePropertyAll(iconColorLT),
        ),
      ),

      //? Border Theme Data
      inputDecorationTheme: InputDecorationTheme(
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey.shade200),
        ),
      ),

      //? Alert Dialog Theme Data

      dialogBackgroundColor: Colors.white,

      //? Elevated Button Theme Data

      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          backgroundColor: MaterialStatePropertyAll(
            kPrimayLightColorLT,
          ),
        ),
      ),
      primaryColor: kPrimayColorLT,
      colorScheme: ColorScheme.fromSeed(
        brightness: Brightness.light,
        seedColor: kPrimayLightColorLT,
        primary: kPrimayLightColorLT,
        primaryContainer: primaryContainerLT,
        background: backgroundColorLT,
        shadow: shadowColorLT,
        secondary: Colors.grey.shade200,
      ),

      //? Text Theme Data

      textTheme: TextTheme(
        labelLarge: TextStyle(
          color: mainTextColorLT,
          fontWeight: FontWeight.bold,
        ),
        labelMedium: TextStyle(
          color: mainTextColorLT,
          fontWeight: FontWeight.bold,
        ),
        labelSmall: TextStyle(
          color: mainTextColorLT,
          fontWeight: FontWeight.bold,
        ),
        bodyLarge: TextStyle(
          color: mainTextColorLT,
          fontWeight: FontWeight.bold,
        ),
        bodyMedium: TextStyle(
          color: mainTextColorLT,
          fontWeight: FontWeight.bold,
        ),
        bodySmall: TextStyle(
          color: mainTextColorLT,
          fontWeight: FontWeight.bold,
        ),
        titleLarge: TextStyle(
          color: mainTextColorLT,
          fontWeight: FontWeight.bold,
        ),
        titleMedium: TextStyle(
          color: mainTextColorLT,
          fontWeight: FontWeight.bold,
        ),
        titleSmall: TextStyle(
          color: mainTextColorLT,
          fontWeight: FontWeight.bold,
        ),
        headlineLarge: TextStyle(
          color: mainTextColorLT,
          fontWeight: FontWeight.bold,
        ),
        headlineMedium: TextStyle(
          color: mainTextColorLT,
          fontWeight: FontWeight.bold,
        ),
        headlineSmall: TextStyle(
          color: mainTextColorLT,
          fontWeight: FontWeight.bold,
        ),
        displayLarge: TextStyle(
          color: mainTextColorLT,
          fontWeight: FontWeight.bold,
        ),
        displayMedium: TextStyle(
          color: mainTextColorLT,
          fontWeight: FontWeight.bold,
        ),
        displaySmall: TextStyle(
          color: mainTextColorLT,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
