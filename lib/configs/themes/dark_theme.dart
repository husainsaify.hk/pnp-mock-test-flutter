import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';

const MaterialColor kPrimayColorDT = MaterialColor(0xFF0CA7FF, {
  50: Color(0xFFF3FBFF),
  100: Color(0xFFE7F7FF),
  200: Color(0xFFC3E9FF),
  300: Color(0xFF9CDBFF),
  400: Color(0xFF55C2FF),
  500: Color(0xFF0CA7FF),
  600: Color(0xFF0B95E3),
  700: Color(0xFF086599),
  800: Color(0xFF064C73),
  900: Color(0xFF04314A),
});

Color kPrimayLightColorDT = const Color(0xFF0CA7FF);
Color appBarIconColorDT = kPrimayColorDT.shade700;
Color backgroundColorDT = kPrimayColorDT.shade900;
Color mainTextColorDT = kPrimayColorDT.shade100;
Color iconColorDT = kPrimayColorDT.shade50;
Color shadowColorDT = kPrimayColorDT.shade600;
Color scaffoldBackgroundColorDT = kPrimayColorDT.shade900;

//?
Color primaryContainerDT = const Color.fromARGB(255, 0, 28, 43);
Color borderColorDT = kPrimayColorDT.shade50.withOpacity(0.2);

class DarkTheme {
  static ThemeData buildDarkTheme() {
    return ThemeData(
      brightness: Brightness.dark,
      scaffoldBackgroundColor: scaffoldBackgroundColorDT,
      shadowColor: shadowColorDT.withOpacity(0.2),
      visualDensity: VisualDensity.adaptivePlatformDensity,
      splashColor: kPrimayColorDT.withOpacity(0.1),
      splashFactory: InkRipple.splashFactory,
      highlightColor: kPrimayColorDT.withOpacity(0.05),

      //? Icon Theme Data

      iconTheme: IconThemeData(
        color: iconColorDT,
      ),

      //? Icon Button Theme Data

      iconButtonTheme: IconButtonThemeData(
        style: ButtonStyle(
          iconColor: MaterialStatePropertyAll(iconColorDT),
        ),
      ),

      //? Alert Dialog Theme Data

      dialogBackgroundColor: Colors.white,

      //? Elevated Button Theme Data

      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          backgroundColor: MaterialStatePropertyAll(
            kPrimayLightColorDT,
          ),
        ),
      ),
      primaryColor: kPrimayColorDT,
      colorScheme: ColorScheme.fromSeed(
        brightness: Brightness.dark,
        seedColor: kPrimayLightColorDT,
        primary: kPrimayLightColorDT,
        primaryContainer: primaryContainerDT,
        background: backgroundColorDT,
        shadow: shadowColorDT,
        secondary: borderColorDT,
      ),

      //? Text Theme Data

      textTheme: TextTheme(
        labelLarge: TextStyle(
          color: mainTextColorDT,
          fontWeight: FontWeight.bold,
        ),
        labelMedium: TextStyle(
          color: mainTextColorDT,
          fontWeight: FontWeight.bold,
        ),
        labelSmall: TextStyle(
          color: mainTextColorDT,
          fontWeight: FontWeight.bold,
        ),
        bodyLarge: TextStyle(
          color: mainTextColorDT,
          fontWeight: FontWeight.bold,
        ),
        bodyMedium: TextStyle(
          color: mainTextColorDT,
          fontWeight: FontWeight.bold,
        ),
        bodySmall: TextStyle(
          color: mainTextColorDT,
          fontWeight: FontWeight.bold,
        ),
        titleLarge: TextStyle(
          color: mainTextColorDT,
          fontWeight: FontWeight.bold,
        ),
        titleMedium: TextStyle(
          color: mainTextColorDT,
          fontWeight: FontWeight.bold,
        ),
        titleSmall: TextStyle(
          color: mainTextColorDT,
          fontWeight: FontWeight.bold,
        ),
        headlineLarge: TextStyle(
          color: mainTextColorDT,
          fontWeight: FontWeight.bold,
        ),
        headlineMedium: TextStyle(
          color: mainTextColorDT,
          fontWeight: FontWeight.bold,
        ),
        headlineSmall: TextStyle(
          color: mainTextColorDT,
          fontWeight: FontWeight.bold,
        ),
        displayLarge: TextStyle(
          color: mainTextColorDT,
          fontWeight: FontWeight.bold,
        ),
        displayMedium: TextStyle(
          color: mainTextColorDT,
          fontWeight: FontWeight.bold,
        ),
        displaySmall: TextStyle(
          color: mainTextColorDT,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
