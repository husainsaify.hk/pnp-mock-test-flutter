import 'dart:developer';

import 'package:mock_test_flutter/configs/app_env.dart';
import 'package:mock_test_flutter/configs/imports/services_imports.dart';

class AuthApi {
  final apiService = ApiBaseClientService(baseUrl);

  Future<String> signIn(String email, String password) async {
    try {
      var data = {'email': email, 'password': password};
      String response = await apiService.post('/login', data);
      return response;
    } catch (e) {
      return "0";
    }
  }

  Future<String> signUp({
    required String name,
    required String email,
    required String password,
  }) async {
    try {
      var data = {'email': email, 'name': name, 'password': password};
      String response = await apiService.post('/register', data);
      return response;
    } catch (e) {
      log("Error :: $e");
      return "0";
    }
  }

  Future<void> updateUserName(
    String authToken,
    String newName,
  ) async {
    try {
      var data = {
        'name': newName,
      };
      var headers = {'Authorization': 'Bearer $authToken'};
      await apiService.post('/update-name', data, headers: headers);
      log("---------------- Name Updated -----------------");
    } catch (e) {
      log("Error :: $e");
      rethrow;
    }
  }

  Future<void> updatePassword(
    String authToken,
    String currentPassword,
    String newPassword,
  ) async {
    try {
      var data = {
        'currunt_password': currentPassword,
        'new_password': newPassword
      };
      var headers = {'Authorization': 'Bearer $authToken'};
      await apiService.post('/change-password', data, headers: headers);
      log("---------------- Change Password -----------------");
    } catch (e) {
      log("Error :: $e");
      rethrow;
    }
  }

  Future<String> forgotPassword(String email, String newPassword) async {
    try {
      var data = {
        'email': email,
        'new_password': newPassword,
      };
      String response = await apiService.post('/forgot-password', data);
      log("---------------- Change Password -----------------");
      return response;
    } catch (e) {
      log("Error :: $e");
      rethrow;
    }
  }

  Future<String> getUserData(String? authToken) async {
    try {
      String response = await apiService
          .get('/me', headers: {'Authorization': 'Bearer $authToken'});
      log("---------------- Getting User Data -----------------");
      return response;
    } catch (e) {
      log("Error :: $e");
      rethrow;
    }
  }
}
