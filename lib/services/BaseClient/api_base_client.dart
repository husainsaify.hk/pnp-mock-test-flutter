import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/utils/constants/const_utils.dart';

class ApiBaseClientService {
  final String baseUrl;

  ApiBaseClientService(this.baseUrl);

  Future<String> get(String endpoint, {Map<String, String>? headers}) async {
    try {
      final response = await http.get(
        Uri.parse('$baseUrl/$endpoint'),
        headers: headers,
      );
      var responseData = jsonDecode(response.body);

      if (response.statusCode == 200) {
        return response.body;
      } else if (response.statusCode >= 400 && response.statusCode < 500) {
        ConstUtils.showShowSnackbar(
          "Error",
          responseData['message'],
          Get.theme.colorScheme.error,
        );
        throw Exception('Bad Request: ${response.body}');
      } else if (response.statusCode >= 500 && response.statusCode < 600) {
        ConstUtils.showShowSnackbar(
          "Error",
          responseData['message'],
          Get.theme.colorScheme.error,
        );
        throw Exception('Internal Server Error: ${response.body}');
      } else {
        ConstUtils.showShowSnackbar(
          "Error",
          responseData['message'],
          Get.theme.colorScheme.error,
        );
        throw Exception(
            'Failed to make GET request. Status code: ${response.statusCode}');
      }
    } catch (e) {
      if (e is SocketException) {
        throw Exception(
            'No internet connection. Please check your connection.');
      } else {
        rethrow;
      }
    }
  }

  Future<String> post(
    String endpoint,
    Map<String, dynamic> data, {
    Map<String, String>? headers,
  }) async {
    try {
      http.Response response = await http.post(
        Uri.parse('$baseUrl$endpoint'),
        headers: headers,
        body: data,
      );

      var responseData = jsonDecode(response.body);
      if (response.statusCode == 200) {
        return response.body;
      } else if (response.statusCode >= 400 && response.statusCode < 500) {
        ConstUtils.showShowSnackbar(
          "Error",
          responseData['message'],
          Get.theme.colorScheme.error,
        );
        throw Exception('Bad Request: ${response.body}');
      } else if (response.statusCode >= 500 && response.statusCode < 600) {
        ConstUtils.showShowSnackbar(
          "Error",
          responseData['message'],
          Get.theme.colorScheme.error,
        );
        throw Exception('Internal Server Error: ${response.body}');
      } else {
        ConstUtils.showShowSnackbar(
          "Error",
          responseData['message'],
          Get.theme.colorScheme.error,
        );
        throw Exception(
            'Failed to make POST request. Status code: ${response.statusCode}');
      }
    } catch (e) {
      if (e is SocketException) {
        throw Exception(
            'No internet connection. Please check your connection.');
      } else {
        rethrow;
      }
    }
  }

  Future<String> resultPost(
    String endpoint,
    Map<String, dynamic> data, {
    Map<String, String>? headers,
  }) async {
    log('$baseUrl$endpoint'.toString());
    log(jsonEncode(data).toString());
    try {
      http.Response response = await http.post(
        Uri.parse('$baseUrl$endpoint'),
        headers: headers,
        body: jsonEncode(data),
      );

      log(response.body);

      var responseData = jsonDecode(response.body);
      if (response.statusCode == 200) {
        return response.body;
      } else if (response.statusCode >= 400 && response.statusCode < 500) {
        ConstUtils.showShowSnackbar(
          "Error",
          responseData['message'],
          Get.theme.colorScheme.error,
        );
        throw Exception('Bad Request: ${response.body}');
      } else if (response.statusCode >= 500 && response.statusCode < 600) {
        ConstUtils.showShowSnackbar(
          "Error",
          responseData['message'],
          Get.theme.colorScheme.error,
        );
        throw Exception('Internal Server Error: ${response.body}');
      } else {
        ConstUtils.showShowSnackbar(
          "Error",
          responseData['message'],
          Get.theme.colorScheme.error,
        );
        throw Exception(
            'Failed to make POST request. Status code: ${response.statusCode}');
      }
    } catch (e) {
      if (e is SocketException) {
        throw Exception(
            'No internet connection. Please check your connection.');
      } else {
        rethrow;
      }
    }
  }
}
