// import 'package:mock_test_flutter/configs/imports/package_imports.dart';

import 'dart:developer';

import 'package:mock_test_flutter/configs/app_env.dart';
import 'package:mock_test_flutter/services/BaseClient/api_base_client.dart';

class QuestionApi {
  //? Base Url

  final apiService = ApiBaseClientService(baseUrl);

  //? Question Set -------------------->

  Future<String> questionSets(int index) async {
    try {
      String response = await apiService.get('/managers_question?page=$index');
      return response;
    } catch (e) {
      log("Error :: $e");
      rethrow;
    }
  }
}
