import 'dart:developer';
import 'package:mock_test_flutter/configs/app_env.dart';
import 'package:mock_test_flutter/configs/imports/services_imports.dart';

class QuizResultApi {
  final apiService = ApiBaseClientService(baseUrl);

  Future<String> fetchQuizResult(
    String? authToken,
    String resultId,
  ) async {
    Map<String, String> headers = {
      'Authorization': 'Bearer $authToken',
    };
    try {
      String response =
          await apiService.get('/result/$resultId', headers: headers);
      return response;
    } catch (e) {
      log("Error :: $e");
      rethrow;
    }
  }

  Future<void> setResult(
    String? authToken,
    Map<String, dynamic> data,
  ) async {
    try {
      log(data.toString());
      Map<String, String> headers = {
        'Authorization': 'Bearer $authToken',
        'Content-type': 'application/json',
      };

      await apiService.resultPost('/generate-result', data, headers: headers);
    } catch (e) {
      log("Error :: $e");
      rethrow;
    }
  }
}
