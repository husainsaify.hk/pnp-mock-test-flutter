import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/models/result_model.dart';

class ResultSummaryController extends GetxController {
  RxInt totalAttemptedQuizzes = 0.obs;
  RxInt totalQuestionCount = 0.obs;
  Rx<double> totalMarksObtained = Rx<double>(0.0);
  RxInt attemptedQuestionsCount = 0.obs;
  RxInt notAttemptedQuestionsCount = 0.obs;
  Rx<double> totalPositiveMarks = Rx<double>(0.0);
  Rx<double> totalNegativeMarks = Rx<double>(0.0);
  RxInt correctAnswersCount = 0.obs;
  RxInt wrongAnswersCount = 0.obs;
  Rx<double> averageMarks = Rx<double>(0.0);

  void calculateSummary(List<ResultModel> resultData) {
    resetValues();
    for (var result in resultData) {
      totalAttemptedQuizzes++;
      totalMarksObtained += result.data.marksObtained;
      totalPositiveMarks += result.data.totalPositiveMarks;
      totalNegativeMarks += result.data.totalNegativeMarks;
      attemptedQuestionsCount += result.data.attemptedQuestionsCount;
      notAttemptedQuestionsCount += result.data.notAttemptedQuestionsCount;

      List<QuestionAnswer> questionAnswers = result.data.questionAnswers;

      for (var answer in questionAnswers) {
        if (answer.marksObtained > 0) {
          correctAnswersCount++;
        } else if (answer.marksObtained < 0) {
          wrongAnswersCount++;
        } else if (answer.marksObtained == 0) {}
      }
    }
    calculateAverageMarks();

    update();
  }

  void calculateAverageMarks() {
    totalQuestionCount.value =
        int.parse((totalAttemptedQuizzes * 10).toString());
    if (totalAttemptedQuizzes.value > 0) {
      averageMarks.value =
          totalMarksObtained.value / totalAttemptedQuizzes.value;
    } else {
      averageMarks.value = 0.0;
    }
  }

  // Reset values to zero
  void resetValues() {
    totalAttemptedQuizzes.value = 0;
    totalMarksObtained.value = 0.0;
    attemptedQuestionsCount.value = 0;
    notAttemptedQuestionsCount.value = 0;
    totalPositiveMarks.value = 0.0;
    totalNegativeMarks.value = 0.0;
    correctAnswersCount.value = 0;
    wrongAnswersCount.value = 0;
  }
}
