import 'package:mock_test_flutter/configs/app_env.dart';
import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class AppLauncherController extends GetxController {
  Future<void> launchPrivacyPolicy(String url) async {
    Uri uri = Uri.parse(url);
    if (await canLaunchUrl(uri)) {
      await launchUrl(uri, mode: LaunchMode.inAppBrowserView);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<void> launchTermsConditions(String url) async {
    Uri uri = Uri.parse(url);
    if (await canLaunchUrl(uri)) {
      await launchUrl(uri, mode: LaunchMode.inAppBrowserView);
    } else {
      throw 'Could not launch $url';
    }
  }

  launchWhatsapp() async {
    var whatsappAndroid =
        Uri.parse("whatsapp://send?phone=+91$whatsappContactNo&text=hello");
    if (await canLaunchUrl(whatsappAndroid)) {
      ConstUtils.showShowSnackbar(
        "Launching...",
        "Whatsapp Launching...",
        Colors.white,
      );
      Future.delayed(
        2000.ms,
        () async => await launchUrl(whatsappAndroid),
      );
    } else {
      ConstUtils.showShowSnackbar(
        "Warning",
        "WhatsApp is not installed on the device",
        Colors.amber.shade700,
      );
    }
  }

  launchCaller() async {
    final Uri url = Uri.parse("tel:+91$contactNo");
    if (await canLaunchUrl(url)) {
      ConstUtils.showShowSnackbar(
        "Launching...",
        "Caller Launching...",
        Colors.white,
      );
      Future.delayed(2000.ms, () async => await launchUrl(url));
    } else {
      ConstUtils.showShowSnackbar(
        "Warning",
        "Caller is not installed on the device",
        Colors.amber.shade700,
      );
    }
  }

  launchEmail() async {
    final url = Uri.parse('mailto:$email?subject=&body=');
    if (await canLaunchUrl(url)) {
      ConstUtils.showShowSnackbar(
        "Launching...",
        "Email Launching...",
        Colors.white,
      );
      Future.delayed(2000.ms, () async => await launchUrl(url));
    } else {
      ConstUtils.showShowSnackbar(
        "Warning",
        "Gmail is not installed on the device",
        Colors.amber.shade700,
      );
    }
  }
}
