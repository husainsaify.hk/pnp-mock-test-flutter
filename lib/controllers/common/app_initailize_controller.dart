import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';

class AppInitializeController extends GetxController {
  final AuthController authController = Get.find();
  final IndexController _indexController = Get.find();

  Future<void> updateQuiz() async {
    DateTime now = DateTime.now();

    final QuestionSetController questionSet = Get.find();

    var time = authController.userData.value!.data.createdAt.toString();

    var diff = DateTime.parse(time).difference(now).inDays * -1;

    if (diff != 100) {
      if (diff == 1 || diff == 2 || diff == 0) {
        _indexController.saveIndex(3);
      } else {
        _indexController.saveIndex(diff);
      }
    } else {
      _indexController.saveIndex(0);
    }

    await questionSet.loadQuestionSetFromPrefs(_indexController.getIndex());
  }
}
