import 'dart:developer';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class ThemeController extends GetxController {
  final Rx<ThemeData> darkTheme = DarkTheme.buildDarkTheme().obs;
  final Rx<ThemeData> lightTheme = LightTheme.buildLightTheme().obs;

  @override
  void onInit() {
    loadThemeMode();
    super.onInit();
  }

  void switchTheme() {
    if (Get.isDarkMode) {
      ConstUtils.showShowSnackbar(
        "Light Mode Activated",
        "Tap again to switch to dark mode",
        Get.theme.colorScheme.background,
      );
    } else {
      ConstUtils.showShowSnackbar(
        "Dark Mode Activated",
        "Tap again to switch to light mode",
        Colors.white,
      );
    }
    GetStorage().write('isDarkMode', Get.isDarkMode); // Save the theme mode
    Get.changeTheme(
      Get.isDarkMode ? lightTheme.value : darkTheme.value,
    );
    Get.changeThemeMode(Get.isDarkMode ? ThemeMode.light : ThemeMode.dark);
    log("Is dark mode :: ${Get.isDarkMode}");
    update();
  }

  Future<void> loadThemeMode() async {
    bool? darkMode = GetStorage().read<bool>('isDarkMode');
    if (darkMode != null) {
      Logger().d("Change Theme :: $darkMode");
      Get.changeTheme(darkMode ? lightTheme.value : darkTheme.value);
    }
    update();
  }
}
