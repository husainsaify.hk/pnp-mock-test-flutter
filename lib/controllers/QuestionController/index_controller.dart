import 'dart:developer';

import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/Storage/question_set_storage.dart';

class IndexController extends GetxController {
  final IndexStorage _indexStorage = IndexStorage();
  RxInt currentIndex = 0.obs;

  IndexController() {
    currentIndex.value = 0;
    saveIndex(currentIndex.value);
  }

  

  void saveIndex(int index) {
    _indexStorage.saveIndex(index);
    update();
  }

  int getIndex() {
    return _indexStorage.getIndex();
  }

  void clearIndex() {
    _indexStorage.saveIndex(0);
    currentIndex.value = 0;
    log("Index cleared".toString());
    update();
  }
}
