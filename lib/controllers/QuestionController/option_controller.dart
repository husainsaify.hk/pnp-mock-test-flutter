import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';

class OptionController extends GetxController {
  bool isSelected = false;
  bool? get _isSelected => isSelected;

  Map<String, dynamic> selectedOptions = <String, dynamic>{}.obs;

  PageController pageController = PageController();

  @override
  void onInit() {
    _getIsSeletected(false);
    super.onInit();
  }

  void _getIsSeletected(bool? isSelected) {
    isSelected = _isSelected;
    update();
  }

  void saveSelectedOption(Map<String, dynamic> result) {
    selectedOptions.assignAll(result);
  }

  void setPageController(int index) {
    pageController.jumpToPage(index);
    Get.back();
  }
}
