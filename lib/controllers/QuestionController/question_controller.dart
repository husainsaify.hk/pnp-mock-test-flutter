import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/controllers/QuestionController/index_controller.dart';
import 'package:mock_test_flutter/models/question_model.dart';

import '../../services/Question/question_api_service.dart';

class QuestionSetController extends GetxController {
  QuestionSet? _questionSet;
  QuestionSet? get questionSet => _questionSet;
  final IndexController _indexController = Get.find();

  @override
  void onInit() {
    super.onInit();
    loadQuestionSetFromPrefs(_indexController.getIndex());
  }

  Future<bool> loadQuestionSetFromPrefs(int index) async {
    String response = await QuestionApi().questionSets(index);
    _questionSet = questionSetFromJson(response);
    update();
    return true;
  }

  Future<QuestionSet?> questionSetFromPrefs(int index) async {
    String response = await QuestionApi().questionSets(index);
    _questionSet = questionSetFromJson(response);
    return _questionSet;
  }
}
