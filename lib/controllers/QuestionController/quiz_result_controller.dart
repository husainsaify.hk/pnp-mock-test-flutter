import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';
import 'package:mock_test_flutter/models/result_model.dart';
import 'package:mock_test_flutter/services/Question/quiz_result_service.dart';

class QuizResultController extends GetxController {
  final QuizResultApi _quizResultApi = QuizResultApi();
  final ResultSummaryController _summaryController = Get.find();
  List<ResultModel> quizResults = <ResultModel>[].obs;

  Future<void> fetchResults(String? authToken, List<String> resultIds) async {
    String response = "";
    if (quizResults.length != resultIds.length) {
      quizResults.clear();
      for (var resultId in resultIds) {
        response = await _quizResultApi.fetchQuizResult(authToken, resultId);
        ResultModel resultModel = resultModelFromJson(response);
        quizResults.add(resultModel);
      }
      _summaryController.calculateSummary(quizResults);
    }

    update();
  }

  Future<bool> setResult(
    OptionController optionController,
    AuthController authController,
  ) async {
    Map<String, dynamic> selectedOptions = optionController.selectedOptions;

    double marksObtained = 0.0;
    int attemptedQuestionsCount = 0;
    int notAttemptedQuestionsCount = 0;
    int wrongAnswersCount = 0;
    int correctAnswersCount = 0;

    //? Logic to create Result ----------->

    for (var element in selectedOptions['question_answer']) {
      if (element['user_answer'] == element['correct_answer']) {
        marksObtained += element['marks_obtained'];
        attemptedQuestionsCount += 1;
        correctAnswersCount += 1;
      } else {
        if (element['user_answer'] != 'F') {
          marksObtained -= 0.5;
          attemptedQuestionsCount += 1;
          wrongAnswersCount += 1;
        } else {
          notAttemptedQuestionsCount += 1;
        }
      }
    }

    selectedOptions['marks_obtained'] = marksObtained;
    selectedOptions['attempted_questions_count'] = attemptedQuestionsCount;
    selectedOptions['not_attempted_questions_count'] =
        notAttemptedQuestionsCount;
    selectedOptions['correct_answers_count'] = correctAnswersCount;
    selectedOptions['wrong_answers_count'] = wrongAnswersCount;
    selectedOptions['total_positive_marks'] = correctAnswersCount;
    selectedOptions['total_negative_marks'] = wrongAnswersCount * 0.5;

    Map<String, dynamic> data = {
      "data": Map<String, dynamic>.from(selectedOptions),
    };

    optionController.saveSelectedOption(selectedOptions);
    await _quizResultApi.setResult(
      authController.authToken,
      data,
    );

    Get.offNamedUntil(
      Routes.resultScreen,
      (route) => false,
    );
    return false;
  }
}
