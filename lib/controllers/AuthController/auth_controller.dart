import 'dart:convert';
import 'dart:developer';

import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/model_imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/services_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class AuthController extends GetxController {
  final AuthApi _authApi = AuthApi();

  final GetStorage _storage = GetStorage();

  Rx<UserModel?> userData = Rx<UserModel?>(null);

  String? authToken;
  final String _message =
      "Oops! Something went wrong? Please try again letter.";

  bool get isLoggedIn => authToken != null;

  @override
  void onInit() {
    super.onInit();
    loadUserData();
  }

  Future<void> loadUserData() async {
    try {
      final QuizResultController quizResult = Get.find();
      authToken = _storage.read('authToken');
      if (authToken != null) {
        String response = await _authApi.getUserData(authToken);

        UserModel user = userModelFromJson(response);
        userData.value = user;
        quizResult.fetchResults(authToken, userData.value!.data.results);
      }
    } catch (error) {
      authToken = null;
      log('Error loading user data: $error');
      ConstUtils.showShowSnackbar(
        "Error",
        "Unable to get the data, please try again.",
        Get.theme.colorScheme.error,
      );
    }
    update();
  }

  Future<bool> signIn(String email, String password) async {
    // Response --------->
    String response = await _authApi.signIn(email, password);
    final responseData = jsonDecode(response);
    if (response != "0") {
      try {
        String authToken = responseData["data"]['token'] ?? "";
        saveUser(authToken);
        loadUserData();

        ConstUtils.showShowSnackbar(
          "Info",
          "Successfully Login",
          Get.theme.colorScheme.primary,
        );

        Future.delayed(
          const Duration(seconds: 1),
          () => Get.offNamedUntil(
            Routes.homePage,
            (route) => false,
          ),
        );
        return false;
      } catch (e) {
        print(e);
        if (responseData['status'] == 0 || response == "0") {
          final errorMessage = responseData["message"];
          ConstUtils.showShowSnackbar(
            "Error",
            errorMessage,
            Get.theme.colorScheme.error,
          );
          return false;
        } else {
          ConstUtils.showShowSnackbar(
            "Error",
            _message,
            Get.theme.colorScheme.error,
          );
        }
        return false;
      }
    } else {
      ConstUtils.showShowSnackbar(
        "Error",
        _message,
        Get.theme.colorScheme.error,
      );
      return false;
    }
  }

  Future<bool> signUp({
    required String name,
    required String email,
    required String password,
  }) async {
    String response =
        await _authApi.signUp(name: name, email: email, password: password);
    final responseData = jsonDecode(response);

    if (response != "0") {
      try {
        String authToken = responseData["data"]['token'] ?? "";
        saveUser(authToken);
        loadUserData();
        String message = responseData['message'];
        ConstUtils.showShowSnackbar(
          "Info",
          message,
          Colors.blue,
        );
        Future.delayed(
          const Duration(seconds: 1),
          () => Get.offNamedUntil(Routes.homePage, (route) => false),
        );
        return false;
      } catch (e) {
        if (responseData['status'] == 0 || response == "0") {
          final errorMessage = responseData["message"];
          ConstUtils.showShowSnackbar(
            "Error",
            errorMessage,
            Get.theme.colorScheme.error,
          );
          return false;
        } else {
          ConstUtils.showShowSnackbar(
            "Error",
            _message,
            Get.theme.colorScheme.error,
          );
          return false;
        }
      }
    } else {
      ConstUtils.showShowSnackbar(
        "Error",
        _message,
        Get.theme.colorScheme.error,
      );
      return false;
    }
  }

  Future<void> changePassword(
    String currentPassword,
    String newPassword,
  ) async {
    try {
      await _authApi.updatePassword(authToken!, currentPassword, newPassword);
      loadUserData();
      ConstUtils.showShowSnackbar(
        "Info",
        "Password changed successfully!",
        Colors.blue,
      );
      Get.back();
    } catch (error) {
      ConstUtils.showShowSnackbar(
        "Info",
        "Failed to change password. Please try again.",
        Colors.blue,
      );
    }
    update();
  }

  Future<void> forgotPassword(
    String currentPassword,
    String newPassword,
  ) async {
    try {
      await _authApi.updatePassword(authToken!, currentPassword, newPassword);
      loadUserData();

      ConstUtils.showShowSnackbar(
        "Info",
        "Password changed successfully!",
        Colors.blue,
      );
      Get.back();
    } catch (error) {
      ConstUtils.showShowSnackbar(
        "Info",
        "Failed to change password. Please try again.",
        Colors.blue,
      );
    }
    update();
  }

  Future<void> updateUserName(String newName) async {
    try {
      await _authApi.updateUserName(authToken!, newName);

      if (userData.value != null) {
        await loadUserData();
        Get.back();
      }

      ConstUtils.showShowSnackbar(
        "Info",
        "Name updated successfully!",
        Colors.blue,
      );
    } catch (error) {
      ConstUtils.showShowSnackbar(
        "Info",
        "Failed to update name. Please try again.",
        Colors.blue,
      );
    }
    update();
  }

  bool signOut() {
    Get.offNamedUntil(Routes.login, (route) => false);

    ConstUtils.showShowSnackbar(
      "Info",
      'Sign Out Successfully',
      Colors.blue,
    );
    Future.delayed(
      const Duration(seconds: 1),
      () async {
        await _storage.remove('authToken');
        authToken = null;
        userData.value = null;
      },
    );
    return true;
  }

  Future<void> saveUser(String authToken) async {
    await _storage.write('authToken', authToken);
    log("Storage Token  :: ${_storage.read('authToken')}");

    update();
  }
}
