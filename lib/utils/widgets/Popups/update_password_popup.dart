import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class UpdatePasswordPopup extends StatefulWidget {
  const UpdatePasswordPopup({super.key});

  @override
  State<UpdatePasswordPopup> createState() => _UpdatePasswordPopupState();
}

class _UpdatePasswordPopupState extends State<UpdatePasswordPopup> {
  final AuthController _authController = Get.find();

  final _currentPasswordC = TextEditingController();
  final _newPasswordC = TextEditingController();

  final _passwordFormKey = GlobalKey<FormState>();

  bool isObscureText = true;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      alignment: Alignment.center,
      backgroundColor: Theme.of(context).colorScheme.background,
      actionsAlignment: MainAxisAlignment.spaceEvenly,
      contentPadding: 0.margin,
      title: Form(
        key: _passwordFormKey,
        child: Column(
          children: [
            Text(
              "Change Password",
              style: Theme.of(context).textTheme.titleLarge,
            ),
            CustomTextFormField(
              controller: _currentPasswordC,
              obscureText: isObscureText,
              title: "title",
              hintText: "Current Password",
              keyboardType: TextInputType.text,
              prifixIcon: Icons.password,
              suffixIcon: null,
              validator: validatePassword,
            ),
            CustomTextFormField(
              controller: _newPasswordC,
              title: "title",
              obscureText: isObscureText,
              hintText: "New Password",
              keyboardType: TextInputType.text,
              prifixIcon: Icons.password_outlined,
              suffixIcon: IconButton(
                onPressed: () => setState(() {
                  isObscureText = !isObscureText;
                }),
                icon: Icon(
                  isObscureText ? Iconsax.eye4 : Iconsax.eye_slash5,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              validator: validatePassword,
            ),
          ],
        ),
      ),
      actions: [
        ProfileDataWidget(
          title: "Cancel",
          onPressed: () => Get.back(),
        ),
        10.width,
        ProfileDataWidget(
          title: "Change",
          onPressed: () {
            if (_passwordFormKey.currentState!.validate()) {
              _authController.changePassword(
                _currentPasswordC.text,
                _newPasswordC.text,
              );
            }
          },
        ),
      ],
    );
  }
}
