import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class AreYouSure extends StatelessWidget {
  AreYouSure({
    super.key,
    required this.ontap,
    required this.title,
    required this.no,
    required this.yes,
  });

  final RxBool isLoading = false.obs;
  final Function() ontap;
  final String title;
  final String no;
  final String yes;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      content: SizedBox(
        height: 180,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              "Are you sure you want to $title",
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.titleLarge,
            ),
            Obx(() {
              return isLoading.value
                  ? CircularProgressIndicator(
                      color: Theme.of(context).primaryColor,
                      strokeWidth: 2,
                    ) // Show loading indicator
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        CustomElevatedButton(
                          onTap: () => Get.back(),
                          title: no,
                        ),
                        10.width,
                        CustomElevatedButton(
                          onTap: () async {
                            isLoading.toggle(); // Start loading
                            await ontap(); // Execute the ontap function
                            isLoading.toggle(); // Stop loading
                          },
                          title: yes,
                        ),
                      ],
                    );
            }),
          ],
        ),
      ),
    );
  }
}
