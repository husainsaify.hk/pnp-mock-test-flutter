import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class UpdateNamePopup extends StatelessWidget {
  UpdateNamePopup({super.key});

  final nameC = TextEditingController();
  final AuthController _authController = Get.find();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      alignment: Alignment.center,
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      actionsAlignment: MainAxisAlignment.spaceEvenly,
      title: Column(
        children: [
          Text(
            "Update Name",
            style: Theme.of(context).textTheme.titleLarge,
          ),
          10.height,
          Text(
            "Your Name : ${_authController.userData.value!.data.name}",
            style: Theme.of(context).textTheme.titleMedium!.copyWith(
                  color: Theme.of(context).colorScheme.primary,
                ),
          ),
          10.height,
          CustomTextFormField(
            controller: nameC,
            obscureText: false,
            title: "title",
            hintText: "New name",
            keyboardType: TextInputType.text,
            prifixIcon: Icons.person_2_outlined,
            suffixIcon: null,
            validator: validateName,
          )
        ],
      ),
      actions: [
        ProfileDataWidget(
          title: "Cancel",
          onPressed: () => Get.back(),
        ),
        10.width,
        ProfileDataWidget(
          title: "Update",
          onPressed: () => _authController.updateUserName(nameC.text),
        ),
      ],
      contentPadding: 0.margin,
    );
  }
}
