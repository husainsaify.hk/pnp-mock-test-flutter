import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class RateUsPopup extends StatefulWidget {
  const RateUsPopup({super.key});

  @override
  State<RateUsPopup> createState() => _RateUsPopupState();
}

class _RateUsPopupState extends State<RateUsPopup> {
  final feedbackController = TextEditingController();

  int selectedIndex = -1;
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      alignment: Alignment.center,
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      actionsAlignment: MainAxisAlignment.spaceEvenly,
      title: Column(
        children: [
          Text(
            "Rate Us",
            style: Theme.of(context).textTheme.titleLarge,
          ),
          SizedBox(
            height: 76,
            width: Get.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: List.generate(
                5,
                (index) => GestureDetector(
                  onTap: () => setState(() {
                    selectedIndex = index;
                  }),
                  child: Container(
                    height: 30,
                    width: 30,
                    margin: EdgeInsets.all(Get.width / (Get.width * 24)),
                    child: selectedIndex + 1 > index
                        ? const Icon(
                            Icons.star_rounded,
                            color: Colors.amber,
                            size: 30,
                          )
                        : Icon(
                            Icons.star_border_rounded,
                            color: Theme.of(context).iconTheme.color,
                            size: 30,
                          ),
                  ),
                ),
              ),
            ),
          ),
          FeedBackForm(
            message: "Feedback",
            controller: feedbackController,
          ),
        ],
      ),
      actions: [
        ProfileDataWidget(
          title: "Rate Us",
          onPressed: () {
            Get.back();
            ConstUtils.showShowSnackbar(
              "Successfully Review Added",
              "Thankyou For Rating",
              Colors.blue,
            );
          },
        ),
      ],
      contentPadding: 0.margin,
    );
  }
}
