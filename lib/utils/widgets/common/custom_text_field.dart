import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class CustomTextFormField extends StatelessWidget {
  const CustomTextFormField({
    super.key,
    required this.controller,
    required this.title,
    required this.hintText,
    required this.keyboardType,
    required this.prifixIcon,
    required this.suffixIcon,
    required this.validator,
    required this.obscureText,
  });

  final String title;
  final String hintText;
  final TextInputType keyboardType;
  final IconData prifixIcon;
  final IconButton? suffixIcon;
  final String? Function(String?)? validator;
  final bool obscureText;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: 0.symmetric(horizontal: 12, vertical: 5),
      child: TextFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        controller: controller,
        keyboardType: keyboardType,
        textInputAction: TextInputAction.next,
        validator: validator,
        obscureText: obscureText,
        onSaved: (email) {},
        style: Theme.of(context).textTheme.bodyLarge!.copyWith(
              fontWeight: FontWeight.w500,
            ),
        decoration: InputDecoration(
          hintText: hintText,
          hintStyle: Theme.of(context)
              .textTheme
              .bodyMedium!
              .copyWith(fontWeight: FontWeight.w500),
          prefixIcon: Padding(
            padding: 12.margin,
            child: Icon(
              prifixIcon,
              color: Theme.of(context).primaryColor,
            ),
          ),
          suffixIcon: suffixIcon,
          border: const UnderlineInputBorder(),
          enabled: true,
          disabledBorder: const UnderlineInputBorder(),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Theme.of(context).colorScheme.primary,
            ),
          ),
        ),
      ),
    );
  }
}
