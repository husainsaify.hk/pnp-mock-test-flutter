import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';

class LogoImage extends StatelessWidget {
  const LogoImage({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: Get.width / 6,
      backgroundColor: Theme.of(context).iconTheme.color!.withOpacity(0.2),
      child: Lottie.asset(
        "assets/profile/profile.json",
        height: Get.width / 6,
      ),
    );
  }
}

class UserProfileWidget extends StatelessWidget {
  const UserProfileWidget({
    super.key,
    required this.radius,
    required this.height,
  });

  final double radius;
  final double height;

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: radius,
      backgroundColor: Colors.black,
      child: Lottie.asset(
        "assets/profile/profile.json",
        height: height,
      ),
    );
  }
}
