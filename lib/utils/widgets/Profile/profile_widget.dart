import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class ProfileFields extends StatelessWidget {
  const ProfileFields({
    super.key,
    required this.onPressed,
    required this.fieldName,
  });

  final Function() onPressed;
  final String fieldName;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        height: 64,
        width: double.maxFinite,
        alignment: Alignment.center,
        margin: const EdgeInsets.only(top: 20),
        decoration: BoxDecoration(
          borderRadius: 12.borderRadius,
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).shadowColor.withOpacity(0.2),
              blurRadius: 5,
              offset: const Offset(2, 2),
            ),
          ],
          color: Theme.of(context).colorScheme.primaryContainer,
        ),
        child: Text(
          fieldName,
          style: Theme.of(context).textTheme.bodyMedium,
        ),
      ),
    );
  }
}

class ProfileDataWidget extends StatelessWidget {
  const ProfileDataWidget({
    super.key,
    required this.title,
    required this.onPressed,
  });

  final Function() onPressed;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48,
      width: Get.width / 3.5,
      decoration: BoxDecoration(
        borderRadius: 12.borderRadius,
        border: Border.all(
          width: 1,
          color: Theme.of(context).colorScheme.primary,
        ),
      ),
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStatePropertyAll(
            Theme.of(context).primaryColor,
          ),
          shape: MaterialStatePropertyAll(
            RoundedRectangleBorder(
              borderRadius: 10.borderRadius,
            ),
          ),
        ),
        onPressed: onPressed,
        child: Text(
          title,
          style: Theme.of(context)
              .textTheme
              .bodyMedium!
              .copyWith(color: Colors.white),
        ),
      ),
    );
  }
}

class ContactUsWidget extends StatelessWidget {
  const ContactUsWidget({
    super.key,
    required this.onTap,
    required this.color,
    required this.icon,
    required this.title,
  });

  final Function() onTap;
  final Color color;
  final IconData icon;
  final String title;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.primaryContainer,
          borderRadius: 12.borderRadius,
          boxShadow: [
            BoxShadow(
              blurRadius: 8,
              color: color.withOpacity(0.15),
              offset: const Offset(2, 2),
            ),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 24,
              backgroundColor: color.withOpacity(0.1),
              child: Icon(
                icon,
                color: color,
              ),
            ),
            5.height,
            Text(
              title,
              style: Theme.of(context).textTheme.bodySmall!.copyWith(
                    fontWeight: FontWeight.bold,
                    color: color,
                  ),
            )
          ],
        ),
      ),
    );
  }
}

class FeedBackForm extends StatefulWidget {
  const FeedBackForm({
    super.key,
    required this.message,
    required this.controller,
  });

  final String message;
  final TextEditingController controller;
  @override
  State<FeedBackForm> createState() => _FeedBackFormState();
}

class _FeedBackFormState extends State<FeedBackForm> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: 0.symmetric(horizontal: 16, vertical: 20),
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.primaryContainer,
        border: Border.all(
          width: 1,
          color: Theme.of(context).primaryColor,
        ),
        boxShadow: [
          BoxShadow(
            offset: const Offset(2, 2),
            color: Theme.of(context).primaryColor.withOpacity(0.2),
            blurRadius: 10,
          )
        ],
        borderRadius: 12.borderRadius,
      ),
      child: Column(
        children: [
          mandatoryLabel(widget.message),
          TextField(
            controller: widget.controller,
            maxLines: widget.message == "Feedback" ? 3 : 6,
            decoration: InputDecoration(
              contentPadding: 0.margin,
              hintText: widget.message,
              border: const OutlineInputBorder(
                borderSide: BorderSide.none,
              ),
            ),
            maxLength: widget.message == "Feedback" ? 100 : 250,
          ),
        ],
      ),
    );
  }
}

Row mandatoryLabel(String label) {
  return Row(
    children: [
      Text(
        label,
        style: Get.theme.textTheme.labelLarge,
      ),
      const Padding(
        padding: EdgeInsets.all(3.0),
      ),
      const Text('*', style: TextStyle(color: Colors.red)),
    ],
  );
}
