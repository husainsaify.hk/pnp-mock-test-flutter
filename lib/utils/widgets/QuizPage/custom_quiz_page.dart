// import 'dart:developer';

import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/model_imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';
import 'package:mock_test_flutter/models/user_model.dart' as user;

class QuestionView extends StatefulWidget {
  const QuestionView({
    super.key,
    required this.quizQuestion,
    required this.pageIndex,
  });

  final Doc? quizQuestion;
  final int pageIndex;

  @override
  State<QuestionView> createState() => _QuestionViewState();
}

class _QuestionViewState extends State<QuestionView> {
  final OptionController _optionController = Get.find();
  final AuthController _authController = Get.find();

  List<Map> quizAnswers = [];

  @override
  Widget build(BuildContext context) {
    user.Data auth = _authController.userData.value!.data;
    return Column(
      children: [
        Text(
          widget.quizQuestion!.text.en,
          style: Theme.of(context).textTheme.titleMedium,
        ),
        10.height,
        Expanded(
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: widget.quizQuestion!.options.length,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: () {
                  onSubmit(
                    widget.quizQuestion!,
                    auth,
                    kvMap[index]!,
                    widget.pageIndex,
                  );
                },
                child: CustomQuizOption(
                  isSelected: isSelected(
                        widget.quizQuestion!.id,
                        _optionController,
                      ) ==
                      index,
                  quizOptions: widget.quizQuestion?.options[index].text,
                  index: index,
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  onSubmit(
    Doc quizQuestion,
    user.Data auth,
    String userChoice,
    int pageIndex,
  ) {
    Map<String, dynamic> selectedOptions = _optionController.selectedOptions;
    double marksObtained = 0;
    setState(() {
      var questionAnswer = {};
      if (userChoice == quizQuestion.correctAnswerId) {
        marksObtained = 1.0;
      } else {
        marksObtained = -0.5;
      }
      if (userChoice !=
          selectedOptions['question_answer'][pageIndex]['user_answer']) {
        questionAnswer = {
          "question_id": quizQuestion.id,
          "correct_answer": quizQuestion.correctAnswerId,
          "user_answer": userChoice,
          "marks_obtained": marksObtained,
        };
      } else {
        questionAnswer = {
          "question_id": quizQuestion.id,
          "correct_answer": quizQuestion.correctAnswerId,
          "user_answer": "F",
          "marks_obtained": marksObtained,
        };
      }

      var userAnswerIndex = selectedOptions['question_answer'].indexWhere(
        (qa) => qa['question_id'] == quizQuestion.id,
      );

      if (userAnswerIndex != -1) {
        selectedOptions['user_id'] = _authController.userData.value!.data.id;
        selectedOptions['question_answer'][userAnswerIndex] = questionAnswer;
      }

      _optionController.saveSelectedOption(selectedOptions);
      print(selectedOptions['question_answer'][pageIndex]);
    });
  }

  String userChoice(int index) {
    switch (index) {
      case 0:
        return 'A';
      case 1:
        return 'B';
      case 2:
        return 'C';
      case 3:
        return 'D';
      default:
        return '';
    }
  }

  final kvMap = {0: 'A', 1: 'B', 2: 'C', 3: 'D'};
}

int isSelected(id, optionController) {
  var selected = optionController.selectedOptions;

  for (var i = 0; i < selected.length; i++) {
    if (selected['question_answer'][i]['question_id'] == id) {
      return indexFromAnswer(
        selected['question_answer'][i]['user_answer'].toString(),
      );
    }
  }

  return -1;
}

int correctedAnswer(id, optionController) {
  var selected = optionController.selectedOptions;

  for (var i = 0; i < selected.length; i++) {
    if (selected['question_answer'][i]['question_id'] == id) {
      return indexFromAnswer(
        selected['question_answer'][i]['correct_answer'].toString(),
      );
    }
  }

  return -1;
}

int indexFromAnswer(String answer) {
  switch (answer) {
    case 'A':
      return 0;
    case 'B':
      return 1;
    case 'C':
      return 2;
    case 'D':
      return 3;
    default:
      return -1;
  }
}
