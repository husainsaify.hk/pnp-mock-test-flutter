import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/app_env.dart';
import 'package:mock_test_flutter/configs/imports/model_imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class CustomQuizOption extends StatefulWidget {
  const CustomQuizOption({
    super.key,
    required this.isSelected,
    required this.quizOptions,
    required this.index,
  });

  final int index;
  final bool isSelected;
  final OptionText? quizOptions;
  @override
  State<CustomQuizOption> createState() => _CustomQuizOptionState();
}

class _CustomQuizOptionState extends State<CustomQuizOption> {
  var selectedIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  List<String> ind = ['A', 'B', 'C', 'D'];

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        10.height,
        Visibility(
          visible: widget.isSelected,
          child: Text(
            "Your Choice",
            style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                  color: Theme.of(context).colorScheme.primary,
                ),
          ),
        ),

        //? Option Container

        LayoutBuilder(
          builder: (context, constraints) => Container(
            width: double.maxFinite,
            constraints: const BoxConstraints(minHeight: 56),
            margin: 0.symmetric(vertical: 5),
            padding: 0.symmetric(vertical: 5),
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.primaryContainer,
              border: Border.all(
                width: 2,
                color: widget.isSelected
                    ? Theme.of(context).colorScheme.primary
                    : Theme.of(context).colorScheme.secondary,
              ),
              borderRadius: 12.borderRadius,
            ),
            child: widget.quizOptions!.imgurl == null
                ? Padding(
                    padding: 0.symmetric(horizontal: 15, vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "${ind[widget.index]}.",
                          style: Theme.of(context).textTheme.titleSmall,
                        ),
                        10.width,
                        Flexible(
                          child: Text(
                            widget.quizOptions!.en,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 8,
                            style: Theme.of(context).textTheme.bodySmall,
                          ),
                        ),
                        20.width,
                        Container(
                          height: 26,
                          width: 26,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: widget.isSelected
                                ? Theme.of(context).colorScheme.primary
                                : Theme.of(context).colorScheme.secondary,
                            border: Border.all(
                              width: 2,
                              color: widget.isSelected
                                  ? Theme.of(context).colorScheme.primary
                                  : Theme.of(context).colorScheme.secondary,
                            ),
                            borderRadius: 20.borderRadius,
                          ),
                          child: Icon(
                            widget.isSelected ? FontAwesomeIcons.check : null,
                            color: widget.isSelected ? Colors.white : null,
                            size: 16,
                          ),
                        ),
                      ],
                    ),
                  )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Container(
                        padding: const EdgeInsets.only(left: 16, top: 16),
                        alignment: Alignment.topLeft,
                        child: Text(
                          "${ind[widget.index]}.",
                          style: Theme.of(context).textTheme.titleSmall,
                        ),
                      ),
                      Container(
                        padding: 12.margin,
                        child: ClipRRect(
                          borderRadius: 20.borderRadius,
                          child: ZoomableImage(
                            imageUrl:
                                "$imageBaseUrl/${widget.quizOptions!.imgurl}",
                          ),
                        ),
                      ),
                      Container(
                        height: 26,
                        width: 26,
                        margin: 8.margin,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: widget.isSelected
                              ? Theme.of(context).colorScheme.primary
                              : Theme.of(context).colorScheme.secondary,
                          border: Border.all(
                            width: 2,
                            color: widget.isSelected
                                ? Theme.of(context).colorScheme.primary
                                : Theme.of(context).colorScheme.secondary,
                          ),
                          borderRadius: 20.borderRadius,
                        ),
                        child: Icon(
                          widget.isSelected ? FontAwesomeIcons.check : null,
                          color: widget.isSelected ? Colors.white : null,
                          size: 16,
                        ),
                      ),
                    ],
                  ),
          ),
        ),
      ],
    );
  }
}

class ZoomableImage extends StatefulWidget {
  final String imageUrl;

  const ZoomableImage({
    super.key,
    required this.imageUrl,
  });

  @override
  // ignore: library_private_types_in_public_api
  _ZoomableImageState createState() => _ZoomableImageState();
}

class _ZoomableImageState extends State<ZoomableImage> {
  final _photoViewController = PhotoViewController();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // onDoubleTap: () => _photoViewController.,
      onTap: () async {
        Get.to(
          () => PhotoView(
            backgroundDecoration: const BoxDecoration(color: Colors.white),
            imageProvider: NetworkImage(widget.imageUrl),
            minScale: PhotoViewComputedScale.contained * 0.8,
            maxScale: PhotoViewComputedScale.covered * 2,
            controller: _photoViewController,
            onTapDown: (context, details, controllerValue) => Get.back(),
          ),
        );
      },
      child: Image.network(
        widget.imageUrl,
        fit: BoxFit.fitWidth,
        errorBuilder: (context, error, stackTrace) {
          return Center(
            child: Text(
              'Image failed to load',
              style: Theme.of(context)
                  .textTheme
                  .titleSmall!
                  .copyWith(color: Colors.black),
            ),
          );
        },
        loadingBuilder: (
          BuildContext context,
          Widget child,
          ImageChunkEvent? loadingProgress,
        ) {
          if (loadingProgress == null) {
            return child;
          }
          return const CircularProgressIndicator();
        },
      ),
    );
  }
}
