import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class ConstUtils {
  static SnackbarController showShowSnackbar(
    String title,
    String message,
    Color? backgroundColor,
  ) {
    return Get.snackbar(
      title,
      message,
      colorText: backgroundColor == Colors.white ||
              backgroundColor == Colors.transparent
          ? Colors.black
          : Colors.white,
      snackPosition: SnackPosition.BOTTOM,
      barBlur: 20,
      snackStyle: SnackStyle.FLOATING,
      margin: 50.margin,
      backgroundColor: backgroundColor,
    );
  }

  static changeTheme() {
    final ThemeController themeController = Get.find();
    return IconButton(
      onPressed: () {
        themeController.switchTheme();
      },
      icon: Icon(
        Get.isDarkMode ? Iconsax.sun_15 : Iconsax.moon5,
        color: Get.theme.iconTheme.color,
      ),
    );
  }
}
