import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';

extension AppUIParameters on num {
  EdgeInsets get margin => EdgeInsets.all(toDouble());
  EdgeInsets get padding => EdgeInsets.all(toDouble());

  EdgeInsets symmetric({double horizontal = 0.0, double vertical = 0.0}) {
    return EdgeInsets.symmetric(horizontal: horizontal, vertical: vertical);
  }

  BorderRadius get borderRadius => BorderRadius.circular(toDouble());

  SizedBox get height => SizedBox(height: toDouble());
  SizedBox get width => SizedBox(width: toDouble());
}
