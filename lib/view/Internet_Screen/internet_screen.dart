import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class InternetScreen extends StatelessWidget {
  const InternetScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Lottie.asset(
              'assets/no_internet/no_internet.json',
              height: 156,
            ),
          ),
          Text(
            "No Internet",
            style: Theme.of(context)
                .textTheme
                .bodyLarge!
                .copyWith(fontWeight: FontWeight.bold),
          ),
          20.height,
          GestureDetector(
            // onTap: () => main(),
            onTap: () {
              Restart.restartApp(webOrigin: Routes.myApp);
            },
            child: Container(
              height: 56,
              width: 120,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                border: Border.all(
                  width: 1,
                  color: Theme.of(context).primaryColor.withOpacity(0.3),
                ),
                boxShadow: const [
                  BoxShadow(
                    offset: Offset(-4, -4),
                    color: Colors.white,
                    blurRadius: 5,
                  ),
                  BoxShadow(
                    offset: Offset(4, 4),
                    color: Colors.black12,
                    blurRadius: 5,
                  )
                ],
                color: Theme.of(context).scaffoldBackgroundColor,
              ),
              child: Text(
                "Try again",
                style: Theme.of(context).textTheme.bodySmall!.copyWith(
                      fontWeight: FontWeight.w700,
                      color: Colors.grey,
                    ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
