import 'dart:developer';

import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class Analytics extends StatefulWidget {
  const Analytics({super.key});

  @override
  State<Analytics> createState() => _AnalyticsState();
}

class _AnalyticsState extends State<Analytics>
    with SingleTickerProviderStateMixin {
  List<double> score = [0.3, 0.4, 0.1, 1.0, 0.9];

  final QuizResultController _quizResult = Get.find();
  final ResultSummaryController _summaryController = Get.find();

  final pageController = PageController();

  int pageIndex = 0;

  List<String> titles = [
    "Total Quiz Attempt",
    "Total Questions Count",
    "Attempt questions",
    "Not attempt questions",
    "Total Mark Obtained",
    "Total Nagitive Marking",
    "Total Positive Marking"
  ];

  @override
  void initState() {
    log("---------------- Analytics -----------------");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = Get.size;
    int startIndex = 0;
    int endIndex = 0;

    return Obx(
      () => SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            10.height,
            Container(
              margin: 0.symmetric(horizontal: 24),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      Text(
                        "Over All Result",
                        style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                              fontSize: Get.width / 30,
                            ),
                      ),
                      10.height,
                      Container(
                        height: Get.width / 2.4,
                        width: Get.width / 2.4,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          borderRadius: 20.borderRadius,
                          color: Theme.of(context).colorScheme.primaryContainer,
                          boxShadow: [
                            BoxShadow(
                              color: Theme.of(context)
                                  .shadowColor
                                  .withOpacity(0.2),
                              blurRadius: 5,
                              offset: const Offset(2, 2),
                            ),
                          ],
                        ),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Text(
                              "${(_summaryController.averageMarks * 10).toStringAsFixed(2)}%\nAverage",
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .copyWith(
                                    fontSize: Get.width / 30,
                                  ),
                            ),
                            CircularProgressIndicator(
                              value: (_summaryController.averageMarks / 10)
                                  .toDouble(),
                              backgroundColor:
                                  Theme.of(context).colorScheme.secondary,
                              strokeAlign: Get.width / (10 * 15),
                              strokeWidth: Get.width / 16,
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Text(
                        "Total Attempted Quiz",
                        style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                              fontSize: Get.width / 30,
                            ),
                      ),
                      10.height,
                      Container(
                        height: Get.width / 2.4,
                        width: Get.width / 2.4,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          borderRadius: 20.borderRadius,
                          color: Theme.of(context).colorScheme.primaryContainer,
                          boxShadow: [
                            BoxShadow(
                              color: Theme.of(context)
                                  .shadowColor
                                  .withOpacity(0.2),
                              blurRadius: 5,
                              offset: const Offset(2, 2),
                            ),
                          ],
                        ),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Text(
                              "${_summaryController.totalAttemptedQuizzes}/100\nSets",
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .copyWith(
                                    fontSize: Get.width / 30,
                                  ),
                            ),
                            CircularProgressIndicator(
                              value: _summaryController.totalAttemptedQuizzes
                                      .toDouble() /
                                  100,
                              backgroundColor:
                                  Theme.of(context).colorScheme.secondary,
                              strokeAlign: Get.width / (10 * 15),
                              strokeWidth: Get.width / 16,
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            15.height,
            Container(
              padding: 24.margin,
              margin: 0.symmetric(horizontal: 24),
              decoration: BoxDecoration(
                borderRadius: 20.borderRadius,
                color: Theme.of(context).colorScheme.primaryContainer,
                boxShadow: [
                  BoxShadow(
                    color: Theme.of(context).shadowColor.withOpacity(0.2),
                    blurRadius: 5,
                    offset: const Offset(2, 2),
                  ),
                ],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  QuizAnalyticsCalculate(
                    title: titles[1],
                    size: size,
                    total: _summaryController.totalQuestionCount,
                  ),
                  QuizAnalyticsCalculate(
                    title: titles[2],
                    size: size,
                    total: _summaryController.attemptedQuestionsCount,
                  ),
                  QuizAnalyticsCalculate(
                    title: titles[3],
                    size: size,
                    total: _summaryController.notAttemptedQuestionsCount,
                  ),
                  QuizAnalyticsCalculate(
                    title: titles[6],
                    size: size,
                    total: _summaryController.totalPositiveMarks,
                  ),
                  QuizAnalyticsCalculate(
                    title: titles[5],
                    size: size,
                    total: _summaryController.totalNegativeMarks,
                  ),
                  QuizAnalyticsCalculate(
                    title: titles[4],
                    size: size,
                    total: _summaryController.totalMarksObtained,
                  ),
                ],
              ),
            ),
            20.height,
            Padding(
              padding: 0.symmetric(horizontal: 24),
              child: Row(
                children: [
                  Text(
                    "Result By Quiz",
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge!
                        .copyWith(fontSize: Get.width / 16),
                  ),
                  const Spacer(),
                  IconButton(
                    onPressed: () {
                      pageController.previousPage(
                        duration: 500.ms,
                        curve: Curves.easeIn,
                      );
                    },
                    icon: const Icon(
                      Iconsax.arrow_circle_left,
                      size: 26,
                    ),
                  ),
                  Text(
                    (pageIndex + 1).toString(),
                    style: Theme.of(context).textTheme.labelLarge,
                  ),
                  IconButton(
                    onPressed: () {
                      pageController.nextPage(
                        duration: 500.ms,
                        curve: Curves.easeIn,
                      );
                    },
                    icon: const Icon(
                      Iconsax.arrow_circle_right,
                      size: 26,
                    ),
                  )
                ],
              ),
            ),
            20.height,
            _quizResult.quizResults.isNotEmpty
                ? Obx(
                    () => SizedBox(
                        height: 98 * 5,
                        child: PageView.builder(
                          onPageChanged: (value) => setState(() {
                            pageIndex = value;
                          }),
                          controller: pageController,
                          itemCount:
                              (_quizResult.quizResults.length / 5).ceil(),
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, pageIndex) {
                            startIndex = pageIndex * 5;
                            endIndex = (pageIndex + 1) * 5;

                            return ListView.builder(
                              cacheExtent: 1,
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount:
                                  endIndex > _quizResult.quizResults.length
                                      ? _quizResult.quizResults.length % 5
                                      : 5,
                              itemBuilder: (context, index) {
                                int itemIndex = startIndex + index;

                                return buildQuizResult(size, context, itemIndex)
                                    .animate()
                                    .moveX(
                                      begin: Get.width,
                                      duration: 300.ms,
                                    )
                                    .fadeIn();
                              },
                            );
                          },
                        )),
                  )
                : Padding(
                    padding: const EdgeInsets.all(50.0),
                    child: Center(
                      child: Text(
                        "No Quiz Attempt",
                        style: Theme.of(context).textTheme.bodyLarge,
                      ),
                    ),
                  ),
            20.height,
          ],
        ),
      ),
    );
  }

  Widget buildQuizResult(Size size, BuildContext context, int index) {
    final data = _quizResult.quizResults[index].data;
    return Container(
      width: Get.width,
      padding: 0.symmetric(horizontal: 20, vertical: 10),
      margin: 0.symmetric(horizontal: 24, vertical: 5),
      decoration: BoxDecoration(
        borderRadius: 20.borderRadius,
        color: Theme.of(context).colorScheme.primaryContainer,
        boxShadow: [
          BoxShadow(
            color: Theme.of(context).shadowColor.withOpacity(0.2),
            blurRadius: 5,
            offset: const Offset(2, 2),
          ),
        ],
      ),
      child: Row(
        children: [
          CircleAvatar(
            radius: Get.width / 16,
            backgroundColor: Theme.of(context).primaryColor,
          ).animate().fadeIn(
                delay: const Duration(milliseconds: 400),
              ),
          10.width,
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "SET ${index + 1}",
                style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                      fontSize: Get.width / 26,
                    ),
              ),
              Text(
                "Mark Obtained : ${(data.marksObtained)}",
                style: Theme.of(context).textTheme.bodySmall!.copyWith(
                      fontSize: Get.width / 34,
                    ),
              ),
              Text(
                "Attempted : ${(data.attemptedQuestionsCount)}",
                style: Theme.of(context).textTheme.bodySmall!.copyWith(
                      fontSize: Get.width / 34,
                    ),
              ),
              Text(
                "Not Attempted : ${(data.notAttemptedQuestionsCount)}",
                style: Theme.of(context).textTheme.bodySmall!.copyWith(
                      fontSize: Get.width / 34,
                    ),
              ),
            ],
          ).animate().fadeIn(
                delay: const Duration(milliseconds: 400),
              ),
          const Spacer(),
          Stack(
            alignment: Alignment.center,
            children: [
              Text(
                "${(data.marksObtained * 10).toStringAsFixed(0)}%",
                style: Theme.of(context).textTheme.bodySmall!.copyWith(
                      fontSize: Get.width / 26,
                    ),
              ).animate().fadeIn(
                    delay: const Duration(milliseconds: 400),
                  ),
              CircularProgressIndicator(
                value: data.marksObtained / 10,
                strokeWidth: Get.width / (36 * 2),
                strokeAlign: Get.width / (30 * 4),
                backgroundColor: Theme.of(context).colorScheme.secondary,
              ).animate().fadeIn(
                    delay: const Duration(milliseconds: 400),
                  ),
            ],
          ),
        ],
      ),
    );
  }
}

class QuizAnalyticsCalculate extends StatelessWidget {
  const QuizAnalyticsCalculate({
    super.key,
    required this.title,
    required this.total,
    required this.size,
  });

  final String title;
  final Size size;
  final Rx total;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          "$title : ",
          style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                fontSize: Get.width / 26,
              ),
        ),
        const Spacer(),
        Text(
          total.value.toString(),
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                fontSize: Get.width / 26,
              ),
        )
      ],
    );
  }
}
