import 'package:mock_test_flutter/configs/app_env.dart';
import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';
import 'package:mock_test_flutter/utils/widgets/common/custom_logo_widget.dart';

import '/configs/imports/view_imports.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  static const String routeName = '/login';

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController emailC = TextEditingController();
  TextEditingController passwordC = TextEditingController();

  final AuthController _authController = Get.find();
  final AppLauncherController _launcherController = Get.find();
  bool isLoading = false;
  bool isObscureText = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: LayoutBuilder(
          builder: (context, viewportConstraints) => SingleChildScrollView(
            padding: 24.margin,
            child: ConstrainedBox(
              constraints: BoxConstraints(
                minHeight: viewportConstraints.maxHeight - 56,
              ),
              child: IntrinsicHeight(
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Row(
                        children: [
                          const Spacer(),
                          ConstUtils.changeTheme(),
                        ],
                      ),
                      20.height,
                      //? App Logo Image ----- >

                      const LogoImage(),

                      //? ------------------- >
                      50.height,
                      Container(
                        width: double.maxFinite,
                        padding: 0.symmetric(horizontal: 10, vertical: 20),
                        decoration: BoxDecoration(
                          color: Theme.of(context).scaffoldBackgroundColor,
                          borderRadius: 20.borderRadius,
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 10,
                              color: Theme.of(context)
                                  .primaryColor
                                  .withOpacity(0.3),
                              offset: const Offset(2, 2),
                            ),
                          ],
                        ),
                        child: Column(
                          children: [
                            Text(
                              'Login',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleLarge!
                                  .copyWith(
                                    color: Theme.of(context).primaryColor,
                                  ),
                            ),
                            10.height,
                            CustomTextFormField(
                              obscureText: false,
                              controller: emailC,
                              title: 'title',
                              hintText: "Your Email",
                              keyboardType: TextInputType.emailAddress,
                              prifixIcon: Icons.alternate_email_outlined,
                              validator: validateEmail,
                              suffixIcon: null,
                            ),
                            10.height,
                            CustomTextFormField(
                              controller: passwordC,
                              obscureText: isObscureText,
                              title: 'title',
                              hintText: "Your password",
                              keyboardType: TextInputType.visiblePassword,
                              prifixIcon: Icons.lock_outlined,
                              suffixIcon: IconButton(
                                onPressed: () => setState(() {
                                  isObscureText = !isObscureText;
                                }),
                                icon: Icon(
                                  isObscureText
                                      ? Iconsax.eye4
                                      : Iconsax.eye_slash5,
                                  color: Theme.of(context).primaryColor,
                                ),
                              ),
                              validator: validatePassword,
                            ),
                            10.height,
                            Align(
                              alignment: Alignment.centerRight,
                              child: TextButton(
                                onPressed: () => showDialog(
                                  context: context,
                                  builder: (context) =>
                                      const ForgotPasswordWidget(),
                                ),
                                child: Text(
                                  "Forget Password",
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleSmall!
                                      .copyWith(
                                        color: Theme.of(context).primaryColor,
                                      ),
                                ),
                              ),
                            ),
                            10.height,
                            CustomButton(
                              onTap: !isLoading ? handleLogin : () {},
                              text: isLoading
                                  ? const Center(
                                      child: CircularProgressIndicator(
                                        color: Colors.white,
                                        strokeWidth: 2,
                                      ),
                                    )
                                  : Text(
                                      "Login",
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleMedium!
                                          .copyWith(
                                            color: Colors.white,
                                          ),
                                    ),
                              backgroundColor: Theme.of(context).primaryColor,
                            ),
                            20.height,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "Don't have an Account?",
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleSmall!
                                      .copyWith(fontWeight: FontWeight.w500),
                                ),
                                TextButton(
                                  onPressed: () {
                                    if (Get.previousRoute == Routes.signUp) {
                                      Get.back();
                                    } else {
                                      Get.toNamed(Routes.signUp);
                                    }
                                  },
                                  child: Text(
                                    "Sign Up",
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleSmall!
                                        .copyWith(
                                          color: Theme.of(context).primaryColor,
                                        ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      const Spacer(),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              " Terms and conditions? ",
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(fontWeight: FontWeight.w500),
                            ),
                            TextButton(
                              onPressed: () => _launcherController
                                  .launchTermsConditions(appTermAndConditions),
                              child: Text(
                                "Click Here",
                                style: Theme.of(context)
                                    .textTheme
                                    .titleSmall!
                                    .copyWith(
                                      color: Colors.blue,
                                    ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  handleLogin() async {
    if (_formKey.currentState!.validate()) {
      setState(() {
        isLoading = true;
      });
      isLoading = await _authController.signIn(
        emailC.text,
        passwordC.text,
      );
    }
    setState(() {});
  }
}
