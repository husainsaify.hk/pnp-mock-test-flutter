import 'package:mock_test_flutter/configs/app_env.dart';
import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';
import 'package:mock_test_flutter/configs/imports/view_imports.dart';
import 'package:mock_test_flutter/utils/widgets/common/custom_logo_widget.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController nameC = TextEditingController();
  TextEditingController emailC = TextEditingController();
  TextEditingController passwordC = TextEditingController();

  final AuthController _authController = Get.find();

  final AppLauncherController _launcherController = Get.find();

  bool isLoading = false;
  bool isObscureText = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: LayoutBuilder(
          builder: (context, viewportConstraints) => SingleChildScrollView(
            padding: 24.margin,
            child: ConstrainedBox(
              constraints: BoxConstraints(
                minHeight: viewportConstraints.maxHeight - 56,
              ),
              child: IntrinsicHeight(
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Row(
                        children: [
                          const Spacer(),
                          ConstUtils.changeTheme(),
                        ],
                      ),
                      20.height,

                      //? App Logo Image ----- >

                      const LogoImage(),

                      //? ------------------- >
                      50.height,
                      Container(
                        width: double.maxFinite,
                        padding: 0.symmetric(horizontal: 10, vertical: 20),
                        decoration: BoxDecoration(
                            borderRadius: 20.borderRadius,
                            color: Theme.of(context).scaffoldBackgroundColor,
                            boxShadow: [
                              BoxShadow(
                                blurRadius: 10,
                                color: Theme.of(context)
                                    .primaryColor
                                    .withOpacity(0.3),
                                offset: const Offset(2, 2),
                              ),
                            ]),
                        child: Column(
                          children: [
                            Text(
                              'Sign Up',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleLarge!
                                  .copyWith(
                                    color: Theme.of(context).primaryColor,
                                  ),
                            ),
                            10.height,
                            CustomTextFormField(
                              obscureText: false,
                              controller: nameC,
                              hintText: "Name",
                              validator: validateName,
                              keyboardType: TextInputType.text,
                              prifixIcon: Icons.person_2_outlined,
                              title: "Name",
                              suffixIcon: null,
                            ),
                            10.height,
                            CustomTextFormField(
                              obscureText: false,
                              controller: emailC,
                              hintText: "Your email",
                              keyboardType: TextInputType.emailAddress,
                              prifixIcon: Icons.alternate_email_outlined,
                              validator: validateEmail,
                              title: "Name",
                              suffixIcon: null,
                            ),
                            10.height,
                            CustomTextFormField(
                              obscureText: isObscureText,
                              controller: passwordC,
                              hintText: "Your Password",
                              keyboardType: TextInputType.visiblePassword,
                              prifixIcon: Icons.lock_outline_rounded,
                              validator: validatePassword,
                              title: "Your Password",
                              suffixIcon: IconButton(
                                onPressed: () => setState(() {
                                  isObscureText = !isObscureText;
                                }),
                                icon: Icon(
                                  isObscureText
                                      ? Iconsax.eye4
                                      : Iconsax.eye_slash5,
                                  color: Theme.of(context).primaryColor,
                                ),
                              ),
                            ),
                            20.height,
                            CustomButton(
                              onTap: !isLoading ? handleSignUp : () {},
                              text: isLoading
                                  ? const Center(
                                      child: CircularProgressIndicator(
                                        strokeWidth: 2,
                                        color: Colors.white,
                                      ),
                                    )
                                  : Text(
                                      "Sign Up",
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleMedium!
                                          .copyWith(
                                            color: Colors.white,
                                          ),
                                    ),
                              backgroundColor: Theme.of(context).primaryColor,
                            ),
                            10.height,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "Already have an Account?",
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleSmall!
                                      .copyWith(fontWeight: FontWeight.w500),
                                ),

                                //? Login Navigate

                                TextButton(
                                  onPressed: navigateToLogin,
                                  child: Text(
                                    "Login",
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleSmall!
                                        .copyWith(
                                          color: Theme.of(context).primaryColor,
                                        ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      const Spacer(),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              " Terms and conditions? ",
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(fontWeight: FontWeight.w500),
                            ),
                            TextButton(
                              onPressed: () => _launcherController
                                  .launchTermsConditions(appTermAndConditions),
                              child: Text(
                                "Click Here",
                                style: Theme.of(context)
                                    .textTheme
                                    .titleSmall!
                                    .copyWith(
                                      color: Colors.blue,
                                    ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  handleSignUp() async {
    if (_formKey.currentState!.validate()) {
      setState(() {
        isLoading = true;
      });
      isLoading = await _authController.signUp(
        name: nameC.text,
        email: emailC.text,
        password: passwordC.text,
      );
    }
    setState(() {});
  }

  navigateToLogin() {
    if (Get.previousRoute == LoginScreen.routeName) {
      Get.back();
    } else {
      Get.toNamed(Routes.login);
    }
  }
}
