import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class ForgotPasswordWidget extends StatefulWidget {
  const ForgotPasswordWidget({super.key});

  @override
  State<ForgotPasswordWidget> createState() => _ForgotPasswordWidgetState();
}

class _ForgotPasswordWidgetState extends State<ForgotPasswordWidget> {
  final AuthController _authController = Get.find();

  final TextEditingController _currentPasswordC = TextEditingController();
  final TextEditingController _newPasswordC = TextEditingController();

  final _passwordFormKey = GlobalKey<FormState>();

  bool isObscureText = true;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      alignment: Alignment.center,
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      actionsAlignment: MainAxisAlignment.spaceEvenly,
      title: Column(
        children: [
          Text(
            "Forgot Password",
            style: Theme.of(context).textTheme.titleLarge,
          ),
          10.height,
          Form(
            key: _passwordFormKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                CustomTextFormField(
                  controller: _currentPasswordC,
                  obscureText: isObscureText,
                  title: "title",
                  hintText: "New password",
                  keyboardType: TextInputType.text,
                  prifixIcon: Icons.password,
                  suffixIcon: null,
                  validator: validatePassword,
                ),
                CustomTextFormField(
                  controller: _newPasswordC,
                  title: "title",
                  obscureText: isObscureText,
                  hintText: "Confirm new password",
                  keyboardType: TextInputType.text,
                  prifixIcon: Icons.password_outlined,
                  suffixIcon: IconButton(
                    onPressed: () => setState(() {
                      isObscureText = !isObscureText;
                    }),
                    icon: Icon(
                      isObscureText ? Iconsax.eye4 : Iconsax.eye_slash5,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  validator: validatePassword,
                ),
                10.height,
              ],
            ),
          )
        ],
      ),
      actions: [
        ProfileDataWidget(
          title: "Cancel",
          onPressed: () => Get.back(),
        ),
        10.width,
        ProfileDataWidget(
          title: "Update",
          onPressed: () {
            if (_passwordFormKey.currentState!.validate()) {
              if (_newPasswordC.text == _currentPasswordC.text) {
                _authController.changePassword(
                  _currentPasswordC.text,
                  _newPasswordC.text,
                );
              }
            }
          },
        ),
      ],
      contentPadding: 0.margin,
    );
  }
}
