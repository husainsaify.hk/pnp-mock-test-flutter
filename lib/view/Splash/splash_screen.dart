import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  static const String routeName = '/splashScreen';

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  final AuthController _authController = Get.find();
  final ConnectionManagerController _connectionManagerController = Get.find();

  @override
  void initState() {
    isLoggedIn();
    super.initState();
  }

  isLoggedIn() {
    int connectionValue = _connectionManagerController.connectionType.value;
    if (connectionValue == 1 || connectionValue == 2) {
      if (_authController.isLoggedIn) {
        navigate(Routes.homePage);
      } else {
        navigate(Routes.signUp);
      }
    } else {
      navigate(Routes.noInternet);
    }
  }

  navigate(String page) {
    return Future.delayed(
      3000.ms,
      () => Get.offNamedUntil(page, (route) => false),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Lottie.asset(
          "assets/profile/profile.json",
          height: Get.width / 16,
        ),
      ),
    );
  }
}
