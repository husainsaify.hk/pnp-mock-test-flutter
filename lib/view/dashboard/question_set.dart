import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class QuestionsSet extends StatefulWidget {
  const QuestionsSet({
    super.key,
    required this.quizName,
    required this.onTap,
  });

  final String quizName;
  final Function() onTap;

  @override
  State<QuestionsSet> createState() => _QuestionsSetState();
}

class _QuestionsSetState extends State<QuestionsSet> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        width: double.maxFinite,
        margin: 0.symmetric(vertical: 10),
        alignment: Alignment.centerLeft,
        padding: 0.symmetric(horizontal: 12, vertical: 10),
        decoration: BoxDecoration(
          borderRadius: 12.borderRadius,
          color: Theme.of(context).colorScheme.primaryContainer,
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).shadowColor.withOpacity(0.2),
              blurRadius: 5,
              offset: const Offset(4, 4),
            ),
          ],
        ),
        child: Row(
          children: [
            Container(
              height: 56,
              width: 56,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: 12.borderRadius,
                color: Theme.of(context).primaryColor.withOpacity(0.1),
              ),
            ),
            10.width,
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.quizName,
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                Text(
                  "QUESTIONS : 10",
                  style: Theme.of(context).textTheme.bodySmall,
                ),
              ],
            ),
            const Spacer(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  "Mark : +1",
                  style: Theme.of(context).textTheme.titleSmall,
                ),
                Text(
                  "Mark : -0.5",
                  style: Theme.of(context).textTheme.titleSmall,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
