import 'dart:developer';

import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/model_imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';
import 'package:mock_test_flutter/configs/imports/view_imports.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({super.key});

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  bool changePremium = false;
  final QuestionSetController _questionSetController = Get.find();
  final QuizResultController _quizResultController = Get.find();
  final IndexController indexController = Get.find();
  final OptionController _optionController = Get.find();
  final AuthController _authController = Get.find();

  @override
  void initState() {
    log("---------------- Dashboard -----------------");
    _optionController.selectedOptions.clear();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Theme.of(context).colorScheme.primaryContainer,
      body: FutureBuilder(
        future: _authController.loadUserData(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: 0.symmetric(horizontal: 24, vertical: 12),
                  padding: 0.symmetric(horizontal: 16, vertical: 12),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Theme.of(context).colorScheme.primaryContainer,
                    boxShadow: [
                      BoxShadow(
                        color: Theme.of(context).shadowColor.withOpacity(0.2),
                        blurRadius: 10,
                      ),
                    ],
                  ),
                  child: Row(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          10.height,
                          Text(
                            Get.isDarkMode.toString(),
                            style: Theme.of(context).textTheme.bodyMedium,
                          ),
                          Obx(
                            () => Text(
                              "${_authController.userData.value!.data.name}✌️",
                              style: Theme.of(context).textTheme.titleLarge,
                            ),
                          ),
                          5.height,
                          Container(
                            height: 26,
                            padding: 0.symmetric(horizontal: 8),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius: 6.borderRadius,
                              color: changePremium
                                  ? const Color(0xffFFB800)
                                  : const Color.fromARGB(255, 255, 17, 0),
                            ),

                            //todo If Premium Available Text and Color Change to Premium and GOLD

                            child: Text(
                              changePremium ? "PREMIUM" : "TRIAL",
                              style: Theme.of(context).textTheme.bodySmall,
                            ),
                          ),
                        ],
                      ),
                      const Spacer(),
                      //todo If Premium Available Colors Change to GOLD

                      IconButton(
                        onPressed: () => setState(() {
                          changePremium = !changePremium;
                        }),
                        icon: Icon(
                          FontAwesomeIcons.solidChessQueen,
                          color: changePremium
                              ? const Color(0xffFFB800)
                              : const Color.fromARGB(255, 255, 17, 0),
                          size: 32,
                        ),
                      ),
                    ],
                  ),
                ).animate().fadeIn(
                      delay: const Duration(milliseconds: 200),
                      duration: const Duration(milliseconds: 600),
                    ),
                Padding(
                  padding: const EdgeInsets.only(left: 24),
                  child: Text(
                    changePremium ? "Today Quiz" : "Trial Quiz",
                    style: Theme.of(context).textTheme.titleLarge,
                  ).animate().fadeIn(
                        duration: const Duration(milliseconds: 600),
                      ),
                ),
                Flexible(
                  child: SingleChildScrollView(
                    child: Container(
                      margin: const EdgeInsets.only(top: 10),
                      padding: 0.symmetric(horizontal: 24),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.vertical(
                          bottom: Radius.zero,
                          top: Radius.circular(20),
                        ),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          //todo If Premium Available Change Trial Question And Remove Trial Mark to Premium

                          changePremium
                              ? QuestionsSet(
                                  onTap: () => showDialog(
                                    context: context,
                                    builder: (context) => AreYouSure(
                                      ontap: () => onSubmit(
                                        indexController.getIndex(),
                                      ),
                                      title: "start the quiz?",
                                      no: "No",
                                      yes: "Yes",
                                    ),
                                  ),
                                  quizName: "SET ${indexController.getIndex()}",
                                ).animate().fadeIn(
                                    duration: const Duration(milliseconds: 600),
                                  )
                              : ListView.builder(
                                  itemCount: 2,
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemBuilder: (context, index) {
                                    return QuestionsSet(
                                      onTap: () => showDialog(
                                        context: context,
                                        builder: (context) => AreYouSure(
                                          ontap: () => onSubmit(index),
                                          title: "start the quiz?",
                                          no: "No",
                                          yes: "Yes",
                                        ),
                                      ),
                                      quizName: "SET ${index + 1}",
                                    ).animate().fadeIn(
                                          duration:
                                              const Duration(milliseconds: 600),
                                        );
                                  },
                                ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            );
          }
        },
      ),
    );
  }

  onSubmit(int index) async {
    print(index);
    bool isFound = false;
    QuestionSet? load =
        await _questionSetController.questionSetFromPrefs(index + 1);

    setState(() {
      if (load != null) {
        for (var i = 0; i < _quizResultController.quizResults.length; i++) {
          for (var quiz in load.questionDetailsPaginated.docs) {
            if (_quizResultController
                    .quizResults[i].data.questionAnswers.isNotEmpty &&
                _quizResultController
                        .quizResults[i].data.questionAnswers[0].questionId ==
                    quiz.id) {
              isFound = true;
              _questionSetController
                  .questionSetFromPrefs(indexController.getIndex());
              log("Already Attempted");
              break;
            }
            break;
          }
        }
      }
    });

    if (!isFound) {
      Get.offNamed(Routes.quizScreen, arguments: {"set_no": index + 1});
    } else {
      Get.back();
      ConstUtils.showShowSnackbar(
        "Info",
        "Already Attempted",
        Colors.blue,
      );
    }
  }
}
