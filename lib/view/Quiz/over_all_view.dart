import 'dart:math' as math;

import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class OverAllView extends StatefulWidget {
  const OverAllView({
    super.key,
  });

  @override
  State<OverAllView> createState() => _OverAllViewState();
}

class _OverAllViewState extends State<OverAllView> {
  final OptionController _optionController = Get.find();
  final AuthController _authController = Get.find();
  final QuizResultController _quizResultController = Get.find();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> selectedOptions = _optionController.selectedOptions;
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar(
        scaffoldKey: _scaffoldKey,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: const Icon(
            FontAwesomeIcons.listCheck,
          ),
        ),
        title: "Quiz Overall Result",
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          padding: 24.margin,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Questions",
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                      fontWeight: FontWeight.w500,
                    ),
              ),
              10.height,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    height: 16,
                    width: 16,
                    decoration: BoxDecoration(
                      borderRadius: 4.borderRadius,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  10.width,
                  Text(
                    "Attemted(${_optionController.selectedOptions['attempted_questions_count']})",
                    style: Theme.of(context).textTheme.bodySmall!.copyWith(
                          fontWeight: FontWeight.w500,
                        ),
                  ),
                  const Spacer(),
                  Container(
                    height: 16,
                    width: 16,
                    decoration: BoxDecoration(
                      borderRadius: 4.borderRadius,
                      color: Theme.of(context).primaryColor.withOpacity(0.3),
                    ),
                  ),
                  10.width,
                  Text(
                    "Not Attemoted (${_optionController.selectedOptions['not_attempted_questions_count']})",
                    style: Theme.of(context).textTheme.bodySmall!.copyWith(
                          fontWeight: FontWeight.w500,
                        ),
                  ),
                ],
              ),
              10.height,
              Align(
                alignment: Alignment.center,
                child: Text(
                  "Total Q : 10",
                  style: Theme.of(context).textTheme.titleMedium,
                ),
              ),
              10.height,
              GridView.builder(
                shrinkWrap: true,
                itemCount: 10,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 25,
                  mainAxisSpacing: 25,
                ),
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      _optionController.setPageController(index);
                      setState(() {});
                    },
                    child: Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: 4.borderRadius,
                        color: selectedOptions.isNotEmpty &&
                                selectedOptions['question_answer'][index]
                                        ['user_answer'] ==
                                    'F'
                            ? Theme.of(context).primaryColor.withOpacity(0.3)
                            : Theme.of(context).primaryColor,
                      ),
                      child: Text(
                        (index + 1).toString(),
                        style: Theme.of(context).textTheme.titleLarge!.copyWith(
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                            ),
                      ),
                    ),
                  );
                },
              ),
              20.height,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  GestureDetector(
                    onTap: () => Get.back(),
                    child: Row(
                      children: [
                        Transform.rotate(
                          angle: 180 * math.pi / 180,
                          child: const Icon(
                            FontAwesomeIcons.rightToBracket,
                          ),
                        ),
                        10.width,
                        Text(
                          "Go Back",
                          style: Theme.of(context).textTheme.titleSmall,
                        ),
                      ],
                    ),
                  ),
                  CustomButton(
                    onTap: () => showDialog(
                      context: context,
                      builder: (context) => AreYouSure(
                        ontap: () => _quizResultController.setResult(
                          _optionController,
                          _authController,
                        ),
                        title: "submit the quiz?",
                        no: 'No',
                        yes: 'Yes',
                      ),
                    ),
                    text: Text(
                      "Submit Quiz",
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(
                            color: Colors.white,
                          ),
                    ),
                    backgroundColor: Theme.of(context).primaryColor,
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Map<int, String> convertToLetters(Map<dynamic, dynamic> numericMap) {
    const List<String> letters = ['A', 'B', 'C', 'D'];

    Map<int, String> letterMap = {};
    numericMap.forEach((key, value) {
      letterMap[key] = letters[value];
    });

    return letterMap;
  }
}
