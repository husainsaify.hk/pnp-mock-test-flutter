import 'package:mock_test_flutter/configs/app_env.dart';
import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/model_imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class QuizExplanation extends StatefulWidget {
  const QuizExplanation({super.key});

  @override
  State<QuizExplanation> createState() => _QuizExplanationState();
}

class _QuizExplanationState extends State<QuizExplanation> {
  final OptionController _optionController = Get.find();
  final QuestionSetController _questionSetController = Get.find();
  final AuthController authController = Get.find();
  bool isExpand = false;
  int activeIndex = -1;

  Color correctColor = const Color.fromARGB(255, 0, 136, 70);
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final selectedOptions = _optionController.selectedOptions;
    final questions =
        _questionSetController.questionSet?.questionDetailsPaginated.docs ?? [];

    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar(
        scaffoldKey: _scaffoldKey,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: const Icon(FontAwesomeIcons.angleLeft),
        ),
        title: "Quiz Explaination",
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          padding: 0.symmetric(horizontal: 24),
          child: ListView.builder(
            itemCount: questions.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              String marksObtained = selectedOptions['question_answer'][index]
                      ['marks_obtained']
                  .toString();
              String correctAnswer =
                  selectedOptions['question_answer'][index]['correct_answer'];
              String userAnswer =
                  selectedOptions['question_answer'][index]['user_answer'];
              return GestureDetector(
                onTap: () => isExpanded(index),
                child: Container(
                  margin: 8.margin,
                  decoration: BoxDecoration(
                    color: userAnswer == correctAnswer
                        ? Colors.green.withOpacity(0.2)
                        : Colors.red.withOpacity(0.2),
                    borderRadius: 12.borderRadius,
                  ),
                  child: ExpansionPanelList(
                    expansionCallback: (panelIndex, _) => isExpanded(index),
                    elevation: 0,
                    materialGapSize: 0,
                    expandedHeaderPadding: 8.margin,
                    children: [
                      ExpansionPanel(
                        canTapOnHeader: true,
                        backgroundColor: Colors.transparent,
                        headerBuilder: (BuildContext context, bool _) {
                          return ConstrainedBox(
                            constraints: const BoxConstraints(minHeight: 76),
                            child: Container(
                              padding:
                                  0.symmetric(horizontal: 24, vertical: 12),
                              alignment: Alignment.centerLeft,
                              child: Row(
                                children: [
                                  userAnswer == correctAnswer
                                      ? const Icon(
                                          Icons.check_circle_rounded,
                                          color: Colors.green,
                                        )
                                      : const Icon(
                                          Iconsax.close_circle5,
                                          color: Colors.red,
                                        ),
                                  10.width,
                                  Flexible(
                                    child: Text(
                                      "${index + 1}. ${questions[index].text.en}",
                                      maxLines: 5,
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleSmall,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                        body: BodyOfExplanation(
                          activeIndex: activeIndex,
                          index: index,
                          marksObtained: marksObtained,
                          selectedOptions: selectedOptions,
                          optionController: _optionController,
                          correctColor: correctColor,
                          questions: questions,
                        ),
                        isExpanded: activeIndex == -1
                            ? activeIndex != -1
                            : activeIndex == index,
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  void isExpanded(int index) {
    return setState(() {
      if (activeIndex == index) {
        activeIndex = -1;
      } else {
        activeIndex = index;
      }
    });
  }
}

class BodyOfExplanation extends StatelessWidget {
  const BodyOfExplanation({
    super.key,
    required this.activeIndex,
    required this.marksObtained,
    required this.selectedOptions,
    required this.correctColor,
    required this.questions,
    required this.index,
    required this.optionController,
  });

  final int activeIndex;
  final int index;
  final String marksObtained;
  final Map<String, dynamic> selectedOptions;
  final OptionController optionController;
  final Color correctColor;
  final List<Doc> questions;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      padding: 0.symmetric(horizontal: 24, vertical: 12),
      decoration: BoxDecoration(
        borderRadius: 12.borderRadius,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                "Mark Obtained : ${marksObtained == 0.5.toString() ? (-0.5).toString() : marksObtained.toString()}",
                style: Theme.of(context).textTheme.titleSmall,
              ),
            ],
          ),
          10.height,
          ListView.builder(
            shrinkWrap: true,
            itemCount: questions[index].options.length,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, int indexO) {
              final questionId =
                  selectedOptions['question_answer'][index]['question_id'];
              final userSelected =
                  isSelected(questionId, optionController) == indexO;
              final correctAnswer =
                  correctedAnswer(questionId, optionController) == indexO;

              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Visibility(
                        visible: userSelected,
                        child: Text(
                          "Your Answer",
                          style:
                              Theme.of(context).textTheme.bodySmall!.copyWith(
                                    color: userSelected == correctAnswer
                                        ? Colors.green
                                        : Colors.red,
                                  ),
                        ),
                      ),
                      const Spacer(),
                      Visibility(
                        visible: correctAnswer,
                        child: Text(
                          "Correct Answer",
                          style:
                              Theme.of(context).textTheme.bodySmall!.copyWith(
                                    color: Colors.green,
                                  ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    width: double.maxFinite,
                    padding: 0.symmetric(horizontal: 12, vertical: 8),
                    margin: 0.symmetric(vertical: 5),
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.primaryContainer,
                      border: Border.all(
                        width: 1,
                        color: correctAnswer
                            ? Colors.green
                            : (userSelected
                                ? Colors.red
                                : Colors.grey.shade300),
                      ),
                      borderRadius: 8.borderRadius,
                    ),
                    child: questions[index].options[indexO].text.imgurl == null
                        ? Text(
                            "${indexO + 1}. ${questions[index].options[indexO].text.en}",
                            style: Theme.of(context).textTheme.titleSmall,
                          )
                        : Container(
                            padding: 12.margin,
                            child: ClipRRect(
                              borderRadius: 20.borderRadius,
                              child: ZoomableImage(
                                imageUrl:
                                    "$imageBaseUrl/${questions[index].options[indexO].text.imgurl}",
                              ),
                            ),
                          ),
                  ),
                ],
              );
            },
          ),
          Container(
            width: double.maxFinite,
            alignment: Alignment.centerLeft,
            padding: 12.padding,
            margin: 0.symmetric(vertical: 5),
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.primaryContainer,
              border: Border.all(
                width: 1,
                color: Theme.of(context).colorScheme.primary,
              ),
              borderRadius: 8.borderRadius,
            ),
            child: Text(
              "Explanation : ${questions[index].explanationInEnglish}",
              style: Theme.of(context).textTheme.titleSmall,
            ),
          ),
        ],
      ),
    );
  }
}
