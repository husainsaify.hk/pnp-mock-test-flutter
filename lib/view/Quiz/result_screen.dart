import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class ResultScreen extends StatefulWidget {
  const ResultScreen({super.key});

  @override
  State<ResultScreen> createState() => _ResultScreenState();
}

class _ResultScreenState extends State<ResultScreen> {
  final OptionController _optionController = Get.find();

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> selectedOptions = _optionController.selectedOptions;
    return PopScope(
      canPop: false,
      onPopInvoked: (didPop) {
        Get.offNamedUntil(Routes.homePage, (route) => false);
      },
      child: Scaffold(
        body: SafeArea(
          child: SizedBox(
            width: double.maxFinite,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  "Quiz Report",
                  style: Theme.of(context).textTheme.headlineLarge!.copyWith(),
                ),
                Lottie.asset(
                  "assets/profile/profile.json",
                  height: Get.width / 16,
                ),
                Text(
                  'Congratulation!',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
                Text(
                  'Certainly! I\'ve made some adjustments to your code to address some issues and improve readability.!',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                Text(
                  'YOUR SCORE',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        letterSpacing: 5,
                      ),
                ),
                Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                        text: selectedOptions['marks_obtained'].toString(),
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                              fontSize: 48,
                              color: selectedOptions['marks_obtained'] <= 5.0
                                  ? Colors.red
                                  : Colors.green,
                            ),
                      ),
                      TextSpan(
                        text: '/10',
                        style: Theme.of(context).textTheme.titleLarge!.copyWith(
                              fontSize: 36,
                            ),
                      ),
                    ],
                  ),
                  textAlign: TextAlign.center,
                ),
                Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                        text: "Correct Answers : ",
                        style: Theme.of(context).textTheme.titleMedium,
                      ),
                      TextSpan(
                        text:
                            selectedOptions['correct_answers_count'].toString(),
                        style: Theme.of(context).textTheme.titleMedium,
                      ),
                    ],
                  ),
                  textAlign: TextAlign.center,
                ),
                Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                        text: "Wrong Answers : ",
                        style: Theme.of(context).textTheme.titleMedium,
                      ),
                      TextSpan(
                        text: selectedOptions['wrong_answers_count'].toString(),
                        style: Theme.of(context).textTheme.titleMedium,
                      ),
                    ],
                  ),
                  textAlign: TextAlign.center,
                ),
                TextButton(
                  onPressed: () => Get.toNamed(
                    Routes.quizExplanation,
                    // (route) => false,
                  ),
                  child: Text(
                    'Result Explanations >>',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(
                          letterSpacing: 2,
                        ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    CustomButton(
                      onTap: () => setState(() {
                        isLoading = true;
                        Future.delayed(const Duration(seconds: 1), () {
                          Get.offNamedUntil(
                            Routes.homePage,
                            (route) => false,
                          );
                          isLoading = false;
                        });
                      }),
                      text: isLoading
                          ? const CircularProgressIndicator(
                              color: Colors.white,
                              strokeWidth: 2,
                            )
                          : Text(
                              "Close",
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(
                                    color: Colors.white,
                                  ),
                            ),
                      backgroundColor: Theme.of(context).primaryColor,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
