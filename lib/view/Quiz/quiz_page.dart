import 'dart:developer';
import 'dart:math' as math;

import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/model_imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';

class QuizScreen extends StatefulWidget {
  const QuizScreen({super.key});

  @override
  State<QuizScreen> createState() => _QuizScreenState();
}

class _QuizScreenState extends State<QuizScreen> {
  int pageIndex = 0;
  bool dataIsAvaliable = false;

  final OptionController _optionController = Get.find();
  final AuthController _authController = Get.find();
  final IndexController _indexController = Get.find();

  late QuestionSetController _questionSetController;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    //? initial Data

    log("------------------ Quiz Page ------------------");

    _questionSetController = Get.find();
    final quizQuestion =
        _questionSetController.questionSet!.questionDetailsPaginated.docs;
    initialData(quizQuestion);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final quizQuestion =
        _questionSetController.questionSet!.questionDetailsPaginated.docs;
    Map<String, dynamic> selectedOptions = _optionController.selectedOptions;
    //?

    final arguments = Get.arguments;
    final int setNo = arguments?['set_no'] ?? 0;

    return PopScope(
      canPop: false,
      child: dataIsAvaliable
          ? Scaffold(
              key: _scaffoldKey,
              appBar: CustomAppBar(
                scaffoldKey: _scaffoldKey,
                leading: IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: const Icon(FontAwesomeIcons.question),
                ),
                title:
                    "Quiz Set : ${setNo == 1 || setNo == 2 ? setNo : _indexController.getIndex()}",
              ),
              body: SafeArea(
                child: Container(
                  margin:
                      const EdgeInsets.only(left: 24, right: 24, bottom: 24),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Text("Questions ${pageIndex + 1}/10",
                              style:
                                  Theme.of(context).textTheme.headlineMedium),
                          const Spacer(),
                          IconButton(
                            onPressed: () {
                              Get.toNamed(
                                Routes.overAllView,
                              );
                            },
                            icon: const Icon(
                              Icons.apps,
                            ),
                          )
                        ],
                      ),
                      10.height,

                      //? Question Indicators ------------>

                      SizedBox(
                        height: 10,
                        child: ListView.builder(
                          itemCount: quizQuestion.length,
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          physics: const NeverScrollableScrollPhysics(),
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              height: 112,
                              width: index == pageIndex ? 26 : 15,
                              margin: 0.symmetric(horizontal: 8),
                              decoration: BoxDecoration(
                                color: index == pageIndex
                                    ? Colors.orange
                                    : selectedOptions.isNotEmpty &&
                                            selectedOptions['question_answer']
                                                    [index]['user_answer'] ==
                                                'F'
                                        ? Colors.grey.withOpacity(0.3)
                                        : Colors.blue,
                                borderRadius: 5.borderRadius,
                              ),
                            );
                          },
                        ),
                      ),
                      30.height,

                      //? Qestion View ------------>

                      Expanded(
                        child: PageView.builder(
                          itemCount: 10,
                          controller: _optionController.pageController,
                          physics: const NeverScrollableScrollPhysics(),
                          onPageChanged: (value) => setState(() {
                            pageIndex = value;
                          }),
                          scrollDirection: Axis.horizontal,
                          pageSnapping: true,
                          itemBuilder: (context, index) {
                            return QuestionView(
                              quizQuestion: quizQuestion[index],
                              pageIndex: pageIndex,
                            );
                          },
                        ),
                      ),

                      //? Bottom Navigations ------------>
                      20.height,

                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Visibility(
                            visible: pageIndex != 0,
                            child: CustomButton(
                              onTap: () {
                                _optionController.pageController.previousPage(
                                  duration: const Duration(milliseconds: 500),
                                  curve: Curves.easeIn,
                                );
                                setState(() {});
                              },
                              text: Text(
                                "Previous",
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium!
                                    .copyWith(
                                      color: Colors.white,
                                    ),
                              ),
                              backgroundColor: Theme.of(context).primaryColor,
                            ),
                          ),
                          Visibility(
                            visible: pageIndex != 0,
                            child: 2.width,
                          ),
                          CustomButton(
                            onTap: () => onSubmit(
                              quizQuestion,
                              pageIndex,
                            ),
                            text: Text(
                              pageIndex == 9 ? "Submit" : "Next",
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(
                                    color: Colors.white,
                                  ),
                            ),
                            backgroundColor: Theme.of(context).primaryColor,
                          )
                        ],
                      ),
                      20.height,
                      GestureDetector(
                        onTap: () => showDialog(
                          context: context,
                          builder: (context) => AreYouSure(
                            ontap: () {
                              Get.offNamedUntil(
                                Routes.homePage,
                                (route) => false,
                              );
                              _optionController.selectedOptions.clear();
                            },
                            title: "quit the quiz?",
                            no: 'No',
                            yes: 'Quit',
                          ),
                        ),
                        child: Container(
                          width: Get.width,
                          color: Colors.transparent,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Transform.rotate(
                                angle: 180 * math.pi / 180,
                                child: const Icon(
                                  FontAwesomeIcons.rightToBracket,
                                ),
                              ),
                              10.width,
                              Text(
                                "Quit Quiz",
                                style: Theme.of(context).textTheme.titleSmall,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          : const Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }

  onSubmit(List<Doc> quizQuestion, int pageIndex) {
    if (pageIndex == 9) {
      Get.toNamed(
        Routes.overAllView,
      );
    } else {
      _optionController.pageController.nextPage(
        duration: const Duration(milliseconds: 500),
        curve: Curves.easeIn,
      );
    }
    setState(() {});
  }

  void initialData(List<Doc> quizQuestion) {
    Map<String, dynamic> selectedOptions = _optionController.selectedOptions;
    List<Map> questionAnswer = [];
    for (var element in quizQuestion) {
      if (selectedOptions.isEmpty) {
        for (var i = 0; i < 10; i++) {}
        questionAnswer.add({
          "question_id": element.id,
          "correct_answer": element.correctAnswerId,
          "user_answer": "F",
          "marks_obtained": 0,
        });
      }
    }

    Map<String, dynamic> result = {
      "user_id": _authController.userData.value!.data.id,
      "total_marks": 10,
      "marks_obtained": 0,
      "attempted_questions_count": 0,
      "not_attempted_questions_count": 0,
      "wrong_answers_count": 0,
      "correct_answers_count": 0,
      "total_positive_marks": 0,
      "total_negative_marks": 0,
      "question_answer": questionAnswer,
    };

    _optionController.saveSelectedOption(result);
    setState(() {
      dataIsAvaliable = true;
    });
  }
}
