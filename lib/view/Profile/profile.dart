import 'package:mock_test_flutter/configs/app_env.dart';
import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/configs/imports/imports.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';
import 'package:mock_test_flutter/utils/widgets/common/custom_logo_widget.dart';

class Profile extends StatefulWidget {
  const Profile({super.key});

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final AuthController _authController = Get.find();
  final AppLauncherController _launcherController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        padding: 24.margin,
        physics: const BouncingScrollPhysics(),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const UserProfileWidget(
                radius: 64,
                height: 76,
              ),
              10.height,
              Obx(() => Column(
                    children: [
                      Text(
                        _authController.userData.value!.data.name,
                        style: Theme.of(context).textTheme.titleLarge,
                      ),
                      5.height,
                      Text(
                        _authController.userData.value!.data.email,
                        style: Theme.of(context).textTheme.labelMedium,
                      ),
                    ],
                  )),
              ProfileFields(
                onPressed: () => showDialog(
                  context: context,
                  builder: (context) => UpdateNamePopup(),
                ),
                fieldName: "Update Name",
              ),
              ProfileFields(
                onPressed: () => showDialog(
                  context: context,
                  builder: (context) => const UpdatePasswordPopup(),
                ),
                fieldName: "Change Password",
              ),
              ProfileFields(
                onPressed: () => showDialog(
                  context: context,
                  builder: (context) => const RateUsPopup(),
                ),
                fieldName: "Rate us",
              ),
              ProfileFields(
                onPressed: () => Get.toNamed(Routes.contactUs),
                fieldName: "Contact us",
              ),
              ProfileFields(
                onPressed: () => _launcherController
                    .launchTermsConditions(appTermAndConditions),
                fieldName: "Terms of use",
              ),
              ProfileFields(
                onPressed: () =>
                    _launcherController.launchTermsConditions(appPrivacyPolicy),
                fieldName: "Privacy Policy",
              ),
              20.height,
              TextButton(
                onPressed: () => showDialog(
                  context: context,
                  builder: (context) => AreYouSure(
                    ontap: () => _authController.signOut(),
                    title: "logout?",
                    no: 'No',
                    yes: 'Logout',
                  ),
                ),
                child: Text(
                  "Logout",
                  style: Theme.of(context)
                      .textTheme
                      .bodyMedium!
                      .copyWith(color: Theme.of(context).primaryColor),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
