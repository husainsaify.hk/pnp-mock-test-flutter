import 'package:flutter/material.dart';
import 'package:mock_test_flutter/configs/imports/package_imports.dart';
import 'package:mock_test_flutter/configs/imports/utils_imports.dart';
import 'package:mock_test_flutter/controllers/common/app_launcher_controller.dart';

class ContactUs extends StatefulWidget {
  const ContactUs({super.key});

  @override
  State<ContactUs> createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  final nameC = TextEditingController();
  final messageController = TextEditingController();
  final AppLauncherController _launcherController = Get.find();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar(
        scaffoldKey: _scaffoldKey,
        leading: Padding(
          padding: const EdgeInsets.only(right: 16.0),
          child: IconButton(
            onPressed: () => Get.back(),
            icon: const Icon(
              FontAwesomeIcons.arrowLeft,
              size: 26,
            ),
          ),
        ),
        title: "Contact Us",
      ),
      body: SingleChildScrollView(
        padding: 24.margin,
        child: Column(
          children: [
            buildContactUsWidgets(Get.size),
            20.height,
            Container(
              width: Get.width,
              padding: 16.margin,
              decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.primaryContainer,
                borderRadius: BorderRadius.circular(24),
                boxShadow: [
                  BoxShadow(
                    offset: const Offset(2, 2),
                    blurRadius: 10,
                    color: Theme.of(context).primaryColor.withOpacity(0.1),
                  ),
                ],
              ),
              child: Column(
                children: [
                  Text(
                    "Quick Contact",
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  10.height,
                  mandatoryLabel("Name"),
                  CustomTextFormField(
                    controller: nameC,
                    title: "name",
                    hintText: "Name",
                    keyboardType: TextInputType.name,
                    prifixIcon: Iconsax.profile_circle5,
                    suffixIcon: null,
                    validator: (validator) {
                      return null;
                    },
                    obscureText: false,
                  ),
                  10.height,
                  mandatoryLabel("Email"),
                  CustomTextFormField(
                    controller: nameC,
                    title: "Email",
                    hintText: "Email",
                    keyboardType: TextInputType.name,
                    prifixIcon: Iconsax.direct_inbox5,
                    suffixIcon: null,
                    validator: (validator) {
                      return null;
                    },
                    obscureText: false,
                  ),
                  24.height,
                  FeedBackForm(
                    message: "Message",
                    controller: messageController,
                  ),
                  24.height,
                  CustomButton(
                    onTap: () {},
                    text: Text(
                      "Send",
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                        color: Colors.white,
                      ),
                    ),
                    backgroundColor: Theme.of(context).primaryColor,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildContactUsWidgets(Size size) {
    List<Map<String, dynamic>> map = [
      {
        "onTap": () => _launcherController.launchCaller(),
        "color": Colors.orange,
        "icon": Iconsax.call_calling5,
        "title": 'Call Us',
      },
      {
        "onTap": () => _launcherController.launchEmail(),
        "color": Colors.blue,
        "icon": Iconsax.direct_inbox5,
        "title": 'Mail Us',
      },
      {
        "onTap": () => _launcherController.launchWhatsapp(),
        "color": Colors.green,
        "icon": FontAwesomeIcons.whatsapp,
        "title": 'Whatsapp',
      },
    ];
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisSpacing: 12,
        mainAxisSpacing: 12,
        // childAspectRatio: 2,
        // mainAxisExtent: 100,
        crossAxisCount: size.width < 250
            ? 1
            : size.width > 250 && size.width <= 300
                ? 2
                : 3,
      ),
      shrinkWrap: true,
      itemCount: 3,
      itemBuilder: (BuildContext context, int index) {
        return ContactUsWidget(
          onTap: map[index]['onTap'],
          color: map[index]['color'],
          icon: map[index]['icon'],
          title: map[index]['title'],
        );
      },
    );
  }
}
