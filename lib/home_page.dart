import 'dart:developer';

import 'package:mock_test_flutter/configs/imports/flutter_imports.dart';
import 'package:mock_test_flutter/utils/widgets/common/custom_logo_widget.dart';

import 'configs/imports/imports.dart';
import 'configs/imports/package_imports.dart';
import 'configs/imports/utils_imports.dart';
import 'configs/imports/view_imports.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int selectedIndex = 0;
  List<String> title = [
    "Dashboard",
    "Analytics",
    "Profile",
  ];
  List<IconData> icon = [
    Icons.apps_rounded,
    Icons.equalizer_rounded,
    Icons.person_2_rounded,
  ];

  final ThemeController themeController = Get.find();
  final AuthController authController = Get.find();

  late ConnectionManagerController _connectionManagerController;
  late AppInitializeController _appInitializeController;

  @override
  void initState() {
    log("---------------- Home Page -----------------");
    _connectionManagerController = Get.find();
    _appInitializeController = Get.find();
    _connectionManagerController.getConnectivityType();
    _appInitializeController.updateQuiz();
    super.initState();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    RxBool onHover = false.obs;
    RxInt count = 0.obs;
    return PopScope(
      canPop: false,
      onPopInvoked: (didPop) {
        showDialog(
          context: context,
          builder: (_) => AreYouSure(
            ontap: () {
              SystemNavigator.pop();
            },
            title: "exit the app?",
            no: 'No',
            yes: 'Yes',
          ),
        );
      },
      child: Scaffold(
        // backgroundColor: Theme.of(context).colorScheme.primaryContainer,
        key: _scaffoldKey,
        extendBodyBehindAppBar: true,
        appBar: CustomAppBar(
          scaffoldKey: _scaffoldKey,
          leading: Icon(
            icon[selectedIndex],
            size: 26,
          ).animate().moveY(
              duration: const Duration(milliseconds: 500), begin: -56, end: 0),
          titleWidget: Container(
            width: Get.width - 50,
            padding: 24.margin,
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    title[selectedIndex],
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ),

                //? Change Theme --------------->

                ConstUtils.changeTheme(),

                //? Change Theme --------------->
                GestureDetector(
                  onTap: () => setState(() {
                    selectedIndex = 2;
                  }),
                  child: const UserProfileWidget(
                    radius: 24,
                    height: 24,
                  ),
                ),
              ],
            ),
          ).animate().moveY(
              duration: const Duration(milliseconds: 500), begin: -56, end: 0),
        ),
        body: SafeArea(
          child: authController.authToken != null
              ? IndexedStack(
                  index: selectedIndex,
                  children: const [
                    Dashboard(),
                    Analytics(),
                    Profile(),
                  ],
                )
              : Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Lottie.asset(
                        "assets/no_internet/try-again.json",
                        height: 256,
                      ),
                      Text(
                        "Something went wrong?",
                        style: Theme.of(context).textTheme.titleMedium,
                      ),
                      10.height,
                      Obx(() => Container(
                            height: 48,
                            width: Get.width / 3,
                            decoration: BoxDecoration(
                              color: onHover.value
                                  ? Theme.of(context)
                                      .colorScheme
                                      .primaryContainer
                                  : Theme.of(context)
                                      .colorScheme
                                      .primaryContainer,
                              borderRadius: 12.borderRadius,
                              border: Border.all(
                                  width: 1,
                                  color: onHover.value
                                      ? Theme.of(context).primaryColor
                                      : Colors.grey),
                            ),
                            child: TextButton(
                              onPressed: () {
                                count.value++;
                                if (count.value == 3 &&
                                    authController.signOut()) {
                                  Get.offNamedUntil(
                                      Routes.login, (route) => false);
                                }
                                authController.loadUserData();
                              },
                              onHover: (value) {
                                onHover.value = value;
                              },
                              style: const ButtonStyle(
                                overlayColor: MaterialStatePropertyAll(
                                  Colors.transparent,
                                ),
                              ),
                              child: Text(
                                "Try again",
                                textAlign: TextAlign.center,
                                style: onHover.value
                                    ? Theme.of(context)
                                        .textTheme
                                        .titleMedium!
                                        .copyWith(
                                            color:
                                                Theme.of(context).primaryColor)
                                    : Theme.of(context)
                                        .textTheme
                                        .titleSmall!
                                        .copyWith(color: Colors.grey),
                              ),
                            ),
                          )),
                    ],
                  ),
                ),
        ),
        bottomNavigationBar: Container(
          height: 84,
          decoration: BoxDecoration(
            color: Theme.of(context).colorScheme.primaryContainer,
            boxShadow: [
              BoxShadow(
                offset: const Offset(-2, 0),
                blurRadius: 10,
                color: Theme.of(context).shadowColor.withOpacity(0.2),
              ),
            ],
            borderRadius: const BorderRadius.vertical(
              top: Radius.circular(20),
            ),
          ),
          child: BottomNavigationBar(
            selectedItemColor: Theme.of(context).colorScheme.primary,
            unselectedItemColor:
                Theme.of(context).colorScheme.primary.withOpacity(0.3),
            backgroundColor: Colors.transparent,
            elevation: 0,
            type: BottomNavigationBarType.fixed,
            onTap: (value) => setState(() {
              selectedIndex = value;
            }),
            currentIndex: selectedIndex,
            showSelectedLabels: false,
            showUnselectedLabels: false,
            items: [
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.apps_rounded,
                  size: selectedIndex == 0 ? 42 : 26,
                ),
                label: "Home",
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.equalizer_rounded,
                  size: selectedIndex == 1 ? 42 : 26,
                ),
                label: "My Tests",
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.person_2_rounded,
                  size: selectedIndex == 2 ? 42 : 26,
                ),
                label: "Profile",
              ),
            ],
          ),
        ).animate().moveY(
              duration: const Duration(milliseconds: 500),
              begin: 100,
              end: 0,
            ),
      ),
    );
  }
}
